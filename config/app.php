<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
     */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
     */

    'url' => 'http://localhost',

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
     */

    'timezone' => 'America/Sao_Paulo',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
     */

    'locale' => 'pt_BR',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
     */

    'fallback_locale' => 'pt_BR',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
     */
    'key' => env('APP_KEY', 'ZQn90sEyEEexl2sIxdVcfSuzLWk56SI9'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
     */

    'log' => 'single',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
     */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Foundation\Providers\ArtisanServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Routing\ControllerServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,

        #acl
        Artesaos\Defender\Providers\DefenderServiceProvider::class,

        #create modules
        Pingpong\Modules\ModulesServiceProvider::class,

/*
Modules\Atividade\Providers\AtividadeServiceProvider::class,
Modules\Boleto\Providers\BoletoServiceProvider::class,
Modules\CaracteristicaFisica\Providers\CaracteristicaFisicaServiceProvider::class,
Modules\Cfop\Providers\CfopServiceProvider::class,
Modules\Chat\Providers\ChatServiceProvider::class,
Modules\Classificacao\Providers\ClassificacaoServiceProvider::class,
Modules\Cliente\Providers\ClienteServiceProvider::class,
Modules\ClienteGrupo\Providers\ClienteGrupoServiceProvider::class,
Modules\ClienteRede\Providers\ClienteRedeServiceProvider::class,
Modules\Cnae\Providers\CnaeServiceProvider::class,
Modules\Colaborador\Providers\ColaboradorServiceProvider::class,
Modules\Conta\Providers\ContaServiceProvider::class,
Modules\Contador\Providers\ContadorServiceProvider::class,
Modules\ContasBancaria\Providers\ContasBancariaServiceProvider::class,
Modules\Contato\Providers\ContatoServiceProvider::class,
Modules\Cor\Providers\CorServiceProvider::class,
Modules\Core\Providers\CoreServiceProvider::class,
Modules\CTe\Providers\CTeServiceProvider::class,
Modules\Email\Providers\EmailServiceProvider::class,
Modules\Endereco\Providers\EnderecoServiceProvider::class,
Modules\Estoque\Providers\EstoqueServiceProvider::class,
Modules\Ferias\Providers\FeriasServiceProvider::class,
Modules\Filial\Providers\FilialServiceProvider::class,
Modules\Financeiro\Providers\FinanceiroServiceProvider::class,
Modules\Fiscal\Providers\FiscalServiceProvider::class,
Modules\Forma\Providers\FormaServiceProvider::class,
Modules\Fornecedor\Providers\FornecedorServiceProvider::class,
Modules\Grupo\Providers\GrupoServiceProvider::class,
Modules\Horario\Providers\HorarioServiceProvider::class,
Modules\Invoice\Providers\InvoiceServiceProvider::class,
Modules\Item\Providers\ItemServiceProvider::class,
Modules\ItemEstoque\Providers\ItemEstoqueServiceProvider::class,
Modules\ItemHistoricoEstoque\Providers\ItemHistoricoEstoqueServiceProvider::class,
Modules\ItemHistoricoValor\Providers\ItemHistoricoValorServiceProvider::class,
Modules\ItemLocacao\Providers\ItemLocacaoServiceProvider::class,
Modules\ItemPergunta\Providers\ItemPerguntaServiceProvider::class,
Modules\ItemProcesso\Providers\ItemProcessoServiceProvider::class,
Modules\ItemRestricao\Providers\ItemRestricaoServiceProvider::class,
Modules\ItemRoyalty\Providers\ItemRoyaltyServiceProvider::class,
Modules\ListaEntrega\Providers\ListaEntregaServiceProvider::class,
Modules\ListaServico\Providers\ListaServicoServiceProvider::class,
Modules\Log\Providers\LogServiceProvider::class,
Modules\Marca\Providers\MarcaServiceProvider::class,
Modules\MDFe\Providers\MDFeServiceProvider::class,
Modules\Motorista\Providers\MotoristaServiceProvider::class,
Modules\Ncm\Providers\NcmServiceProvider::class,
Modules\NFe\Providers\NFeServiceProvider::class,
Modules\NFse\Providers\NFseServiceProvider::class,
Modules\Orcamento\Providers\OrcamentoServiceProvider::class,
Modules\OrdemCompra\Providers\OrdemCompraServiceProvider::class,
Modules\OrdemProducao\Providers\OrdemProducaoServiceProvider::class,
Modules\OrdemServico\Providers\OrdemServicoServiceProvider::class,
Modules\Pedido\Providers\PedidoServiceProvider::class,
Modules\Permissao\Providers\PermissaoServiceProvider::class,
Modules\Pet\Providers\PetServiceProvider::class,
Modules\PlanoConta\Providers\PlanoContaServiceProvider::class,
Modules\PlanoContaCategoria\Providers\PlanoContaCategoriaServiceProvider::class,
Modules\PlanoContaCentro\Providers\PlanoContaCentroServiceProvider::class,
Modules\PlanoContaCusto\Providers\PlanoContaCustoServiceProvider::class,
Modules\Ponto\Providers\PontoServiceProvider::class,
Modules\Portaria\Providers\PortariaServiceProvider::class,
Modules\Porte\Providers\PorteServiceProvider::class,
Modules\Prazo\Providers\PrazoServiceProvider::class,
Modules\Processo\Providers\ProcessoServiceProvider::class,
Modules\Produto\Providers\ProdutoServiceProvider::class,
Modules\Profissao\Providers\ProfissaoServiceProvider::class,
Modules\Projeto\Providers\ProjetoServiceProvider::class,
Modules\Recibo\Providers\ReciboServiceProvider::class,
Modules\ReferenciaComercial\Providers\ReferenciaComercialServiceProvider::class,
Modules\Royalty\Providers\RoyaltyServiceProvider::class,
Modules\Segmento\Providers\SegmentoServiceProvider::class,
Modules\Setor\Providers\SetorServiceProvider::class,
Modules\Sms\Providers\SmsServiceProvider::class,
Modules\Tabela\Providers\TabelaServiceProvider::class,
Modules\TabelaPreco\Providers\TabelaPrecoServiceProvider::class,
Modules\Tarefas\Providers\TarefasServiceProvider::class,
Modules\Telemarketing\Providers\TelemarketingServiceProvider::class,
Modules\Transportadora\Providers\TransportadoraServiceProvider::class,
Modules\Tratamento\Providers\TratamentoServiceProvider::class,
Modules\Usuario\Providers\UsuarioServiceProvider::class,
 */
        #migrate generator
        Way\Generators\GeneratorsServiceProvider::class,
        Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class,

        #Validaion
        KennedyTedesco\Validation\ValidationServiceProvider::class,

        #Laravel MongoDB (Eloquent)
        Jenssegers\Mongodb\MongodbServiceProvider::class,

        #Debugbar
        Barryvdh\Debugbar\ServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
     */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'Redis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,

        'Entrust' => Zizaco\Entrust\EntrustFacade::class,

        'Module'   => Pingpong\Modules\Facades\Module::class,
        'Defender' => \Artesaos\Defender\Facades\Defender::class,

        'JWTAuth'    => Tymon\JWTAuth\Facades\JWTAuth::class,
        'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class,

        #Mongodb
        'Moloquent' => Jenssegers\Mongodb\Model::class,

        #Debugbar
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
    ],

];
