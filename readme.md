Sistema LS
========

#### Índice

* [Instalação](#markdown-header-instalacao)
    * [Requisitos](#markdown-header-requisitos)
* [Configuração](#markdown-header-configuracao)


Instalação
==========

### Requisitos

1. [Instalar Vagrant](http://docs.vagrantup.com/v2/installation/)
2. [Instalar Homestead](http://laravel.com/docs/5.0/homestead#installation-and-setup)
3. [Configurar Homestead](#markdown-header-configurar-homestead)
4. [Clonando Repositório](#markdown-header-clonando-repositorio)
5. [Configurar Hosts](#markdown-header-configurar-hosts)
6. [Configurar Laravel](#markdown-header-configurar-laravel)

### Configurar Homestead

Use `homestead edit` para alterar o arquivo de configuração, em sites deixe parecido com isso:

```
sites:
    - map: sistema.dev
      to: /home/vagrant/Code/sistema
```

### Clonando repositório

Entre pelo SSH no Homestead:

`homestead ssh`

Vá para `~/Code`:

`cd ~/Code`

Então clone o repositório:

```git clone https://bitbucket.org/lstech/sistema.git```

### Configurar Hosts

No Windows, edite o arquivo em `C:\Windows\system32\drivers\etc\hosts`.

No Linux ou Mac, edite o arquivo `/etc/hosts`.

Adicione a linha no final do arquivo:

```
# Sistema LS
192.168.10.10 sistema.dev
```

### Configurar Laravel

1 - Crie um arquivo chamado `.env` na raiz do projeto com o seguinte conteúdo:

```
APP_ENV=local
APP_DEBUG=true
APP_KEY=devsistema

DB_HOST=localhost
DB_DATABASE=sistema
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=smtp.lschurch.com.br
MAIL_PORT=587
MAIL_USERNAME=dev@lschurch.com.br
MAIL_PASSWORD=ZphXHp9nc458
```