var elixir = require('laravel-elixir');

var bowerPath = '../bower/';

elixir(function(mix) {
    mix
        .sass([
            'dependencies.scss',
            bowerPath + 'font-awesome/scss/font-awesome.scss',
        ], 'public/assets/css/dependencies.css')
        .sass([
            'tema/smartadmin-production.scss',
            'tema/smartadmin-production-plugins.scss',
            'tema/smartadmin-skins.scss'
        ], 'public/assets/css/tema.css')
        .sass([
            'app.scss',
            bowerPath + 'bootstrap-sweetalert/lib/sweet-alert.css'
            ], 'public/assets/css/app.css')
    // Scripts
        .scripts([
             bowerPath + 'jquery/dist/jquery.js',
             bowerPath + 'angular/angular.min.js',
             bowerPath + 'angular-i18n/angular-locale_pt-br.js',
             bowerPath + 'angular-bootstrap-show-errors/src/showErrors.js',
             bowerPath + 'angular-ui-router/release/angular-ui-router.js',
             bowerPath + 'satellizer/satellizer.js',
             bowerPath + 'bootstrap-sass/assets/javascripts/bootstrap.js',
             bowerPath + 'bootstrap-sweetalert/lib/sweet-alert.js',
            ], 'public/assets/js/dependencies.js')
        .scripts(['**/*.js'], 'public/assets/js/app.js')
    // Version
        .version([
            'public/assets/css/dependencies.css',
            'public/assets/css/tema.css',
            'public/assets/css/app.css',
            'public/assets/js/dependencies.js',
            'public/assets/js/app.js'
        ])
    // Move assets
        .copy('resources/assets/bower/font-awesome/fonts/*.{eot,svg,ttf,woff,woff2}', 'public/assets/fonts/');
});

