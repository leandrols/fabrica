<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$faker = Faker\Factory::create('pt_BR');

$factory->define(Modules\Usuario\Entities\Usuario::class, function () use ($faker) {
    return [
        'nome'           => $faker->firstName,
        'sobrenome'      => $faker->lastName,
        'login'          => $faker->username,
        'email'          => $faker->email,
        'senha'          => '123qwe',
        'colaborador_id' => rand(1, 20),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Modules\Permissao\Models\Permissao::class, function () use ($faker) {
    return [
        'name'          => $faker->firstname,
        'readable_name' => $faker->name,
    ];
});

$factory->define(Modules\Endereco\Entities\Endereco::class, function () use ($faker) {

    return [
        'nome'        => $faker->firstName,
        'cep'         => str_random(8),
        'logradouro'  => substr($faker->address, 0, 69),
        'numero'      => rand(1, 20),
        'complemento' => $faker->firstName,
        'bairro'      => $faker->firstName,
        'cidade_id'   => rand(1, 20),
        'tipo'        => $faker->randomElement($array = array('Pessoal', 'Entrega', 'Cobranca')),
    ];
});

$factory->define(Modules\Endereco\Entities\Cidade::class, function () use ($faker) {
    return [
          'nome'             => $faker->name,
          'cep'              => $faker->randomDigit(8),
          'ibge'             => rand(1, 20),
          'ibge_verificador' => rand(1, 20),
          'uf_id'            => rand(1, 20),
    ];
});

$factory->define(Modules\Endereco\Entities\Uf::class, function () use ($faker) {
    return [
          'nome'     => $faker->firstName,
          'sigla'    => 'PR',
          'regiao'   => $faker->name,
          'aliquota' => rand(1, 20),
          'ibge'     => rand(1, 20),
          'pais_id'  => rand(1, 20),
    ];
});

$factory->define(Modules\Endereco\Entities\Pais::class, function () use ($faker) {
    return [
        'nome'        => $faker->name,
        'complemento' => $faker->name,
    ];
});

$factory->define(Modules\ContasBancaria\Entities\ContasBancaria::class, function () use ($faker) {
    return [
          'nome'       => $faker->firstName,
          'favorecido' => $faker->name,
          'banco'      => '0341',
          'agencia'    => '0321',
          'conta'      => '721557',
    ];
});

$factory->define(Modules\Tabela\Entities\Tabela::class, function () use ($faker) {
    return [
          'id' => 1,
    ];
});

$factory->define(Modules\Transportadora\Entities\Transportadora::class, function () use ($faker) {
    return [
      'razao'       => $faker->name,
      'fantasia'    => $faker->firstName,
      'cnpj'        => '12345678901234',
      'ie'          => 'isento',
      'endereco_id' => rand(1, 20),
      'telefone'    => '4133334444',
      'email'       => $faker->email,
      'site'        => 'http://'.$faker->firstName.'.com',
      'obs'         => $faker->sentence,
      ];
});

$factory->define(Modules\Atividade\Entities\Atividade::class, function () use ($faker) {
    return [
          'id' => 1,
    ];
});

$factory->define(Modules\ClienteGrupo\Entities\ClienteGrupo::class, function () use ($faker) {
    return [
          'nome' => $faker->name,
    ];
});

$factory->define(Modules\ClienteRede\Entities\ClienteRede::class, function () use ($faker) {
    return [
          'nome' => $faker->name,
    ];
});

$factory->define(Modules\Porte\Entities\Porte::class, function () use ($faker) {
    return [
          'nome'   => $faker->name,
          'fatura' => $faker->randomDigit(4),
    ];
});

$factory->define(Modules\Forma\Entities\Forma::class, function () use ($faker) {
    return [
      'nome'     => $faker->firstName,
      'conta_id' => rand(1, 20),
 //     'carencia'                     => 0,
      'mora' => 1,
//      'ordem'                        => $faker->randomDigit,
      'despesa_plano_conta_custo_id' => 3,
      'despesa_margem'               => 1,
      'despeza_valor'                => 1,
      'valor_minimo'                 => 1,
      'smart_phone'                  => true,
    ];
});

$factory->define(Modules\Filial\Entities\Filial::class, function () use ($faker) {
    return [
      'id' => 1,
    ];
});

$factory->define(Modules\Profissao\Entities\Profissao::class, function () use ($faker) {
    return [
      'id' => 1,
    ];
});

$factory->define(Modules\Conta\Entities\Conta::class, function () use ($faker) {
    return [
      'nome'              => $faker->name,
      'saldo'             => 1000,
      'saldo_moeda1'      => 1000,
      'saldo_moeda2'      => 1000,
      'filial_id'         => 1,
      'conta_contabil'    => str_random(20),
      'fluxo'             => true,
      'conta_bancaria_id' => rand(1, 20),
    ];
});

$factory->define(Modules\PlanoContaCusto\Entities\PlanoContaCusto::class, function () use ($faker) {
    return [
      'nome'                  => $faker->firstName,
      'plano_conta_centro_id' => rand(1, 20),
      'valor_ideal'           => $faker->randomDigit,
      'conta_contabil'        => $faker->firstName,
      'classificacao'         => 'xxx',
    ];
});

$factory->define(Modules\PlanoContaCentro\Entities\PlanoContaCentro::class, function () use ($faker) {
    return [
      'plano_conta_categoria_id' => rand(1, 20),
      'classificacao'            => 'xxx',
      'mercadoria'               => true,
    ];
});

$factory->define(Modules\PlanoContaCategoria\Entities\PlanoContaCategoria::class, function () use ($faker) {
    return [
      'plano_conta_id' => rand(1, 20),
      'nome'           => $faker->firstName,
      'classificacao'  => 'xxx',
    ];
});

$factory->define(Modules\PlanoConta\Entities\PlanoConta::class, function () use ($faker) {
    return [
      'nome'          => $faker->firstName,
      'classificacao' => 'xxx',
    ];
});

$factory->define(Modules\Colaborador\Entities\Colaborador::class, function () use ($faker) {
    return [
              'endereco_id'                   => rand(1, 20),
              'assinatura'                    => $faker->name,
              'bate_ponto'                    => true,
              'celular'                       => '3488041263',
              'comissionado'                  => true,
              'comissao_base'                 => $faker->randomElement($array = array('Nenhuma', 'Desconto', 'Lucro')),
              'carteira_reservista'           => $faker->firstName,
              'carteira_reservista_categoria' => $faker->firstName,
              'carteira_trabalho'             => $faker->firstName,
              'carteira_trabalho_data'        => $faker->date,
              'carteira_trabalho_serie'       => str_random(3),
              'carteira_trabalho_uf'          => 'PR',
              'conta_bancaria_id'             => rand(1, 20),
              'cpf'                           => str_random(11),
              'data_admissao'                 => $faker->date,
              'data_nascimento'               => $faker->date,
              'dia_pagamento'                 => rand(1, 30),
              'email'                         => $faker->email,
              'email_comercial'               => $faker->email,
              'email_id'                      => rand(1, 20),
              'escolaridade'                  => $faker->randomElement($array = array('Solteiro', 'Casado', 'Viuvo', 'Divorciado', 'Separado')),
              'caracteristicas_fisica_id'     => rand(1, 20),
              'horario_id'                    => rand(1, 20),
              'filial_id'                     => 1,
              'profissao_id'                  => 1,
              'meta_positiva'                 => 5,
              'nascimento_cidade_id'          => rand(1, 20),
              'nome'                          => substr($faker->name, 0, 49),
              'rg'                            => substr($faker->firstName, 0, 9),
              'sexo'                          => $faker->randomElement($array = array('Masculino', 'Feminino')),
              'smart_phone'                   => (rand(0, 1)) ? true : false,
              'status'                        => (rand(0, 1)) ? true : false,
              'vic'                           => (rand(0, 1)) ? true : false,
    ];
});

$factory->define(Modules\Email\Entities\Email::class, function () use ($faker) {
    return [
          'nome'          => $faker->name,
          'host_entrada'  => $faker->name,
          'porta_entrada' => 995,
          'conta_entrada' => $faker->email,
          'email_entrada' => $faker->email,
          'senha_entrada' => str_random(80),
          'host_saida'    => $faker->name,
          'porta_saida'   => 465,
          'conta_saida'   => $faker->email,
          'email_saida'   => $faker->email,
          'senha_saida'   => str_random(80),
          'ssl'           => true,
          'protocolo'     => 'tsl',
    ];
});

$factory->define(Modules\CaracteristicaFisica\Entities\CaracteristicaFisica::class, function () use ($faker) {
    return [
            'altura'     => 173,
            'cabelo'     => 'Preto',
            'deficiente' => false,
            'olhos'      => 'Castanho',
            'peso'       => 110,
            'raca'       => 'Pardo',
            'sangue'     => 'AB',
            'rh'         => true,
            'sinais'     => 'Nenhum',
    ];
});

$factory->define(Modules\Horario\Entities\Horario::class, function () use ($faker) {
    return [
          'refeicao_inicio_dom'  => $faker->time,
          'refeicao_inicio_seg'  => $faker->time,
          'refeicao_inicio_ter'  => $faker->time,
          'refeicao_inicio_qua'  => $faker->time,
          'refeicao_inicio_qui'  => $faker->time,
          'refeicao_inicio_sex'  => $faker->time,
          'refeicao_inicio_sab'  => $faker->time,
          'refeicao_fim_dom'     => $faker->time,
          'refeicao_fim_qua'     => $faker->time,
          'refeicao_fim_qui'     => $faker->time,
          'refeicao_fim_sab'     => $faker->time,
          'refeicao_fim_seg'     => $faker->time,
          'refeicao_fim_sex'     => $faker->time,
          'inicio_dom'           => $faker->time,
          'inicio_qua'           => $faker->time,
          'inicio_qui'           => $faker->time,
          'inicio_sab'           => $faker->time,
          'inicio_seg'           => $faker->time,
          'inicio_sex'           => $faker->time,
          'inicio_ter'           => $faker->time,
          'fim_dom'              => $faker->time,
          'fim_qua'              => $faker->time,
          'fim_qui'              => $faker->time,
          'fim_sab'              => $faker->time,
          'fim_seg'              => $faker->time,
          'fim_sex'              => $faker->time,
          'fim_ter'              => $faker->time,
          'intervalo_inicio_dom' => $faker->time,
          'intervalo_inicio_qua' => $faker->time,
          'intervalo_inicio_qui' => $faker->time,
          'intervalo_inicio_sab' => $faker->time,
          'intervalo_inicio_seg' => $faker->time,
          'intervalo_inicio_sex' => $faker->time,
          'intervalo_inicio_ter' => $faker->time,
          'intervalo_fim_dom'    => $faker->time,
          'intervalo_fim_qua'    => $faker->time,
          'intervalo_fim_qui'    => $faker->time,
          'intervalo_fim_sab'    => $faker->time,
          'intervalo_fim_seg'    => $faker->time,
          'intervalo_fim_sex'    => $faker->time,
          'intervalo_fim_ter'    => $faker->time,
          'entrada_extra'        => 5,
          'tolerancia_entrada'   => 5,
          'tolerancia_refeicao'  => 5,
          'tolerancia_saida'     => 5,
          'banco_horas'          => true,
          'hora_extra'           => true,
          'extra_minimo'         => 5,
          'intervalo_automatico' => true,
      ];
});

$factory->define(Modules\Setor\Entities\Setor::class, function () use ($faker) {
    return [
            'nome' => $faker->name,
    ];
});

$factory->define(Modules\Prazo\Entities\Prazo::class, function () use ($faker) {
    return [
            'forma_id'     => rand(1, 20),
            'nome'         => $faker->name,
            'valor_minimo' => rand(5, 50),
    ];
});

$factory->define(Modules\Cliente\Entities\Cliente::class, function () use ($faker) {
  $status = array();
  $status[1] = 'Ativo';
  $status[2] = 'Inativo';
  $status[3] = 'Bloqueado';
  $status[4] = 'A vista';

  $diaSemana = array();
  $diaSemana[1] = 'Domingo';
  $diaSemana[2] = 'Segunda';
  $diaSemana[3] = 'Terça';
  $diaSemana[4] = 'Quarta';
  $diaSemana[5] = 'Quinta';
  $diaSemana[6] = 'Sexta';
  $diaSemana[7] = 'Sábado';

    return [
            'razao'                           => $faker->name,
            'fantasia'                        => $faker->name,
            'cnpj'                            => str_random(14),
            'ie'                              => 'isento',
            'endereco_id'                     => rand(1, 20),
            'cobranca_endereco_id'            => rand(1, 20),
            'email'                           => $faker->email,
            'telefone'                        => '4141414141',
            'ramal'                           => '10009',
            'forma_id'                        => rand(1, 20),
            'vendedor_colaborador_id'         => rand(1, 20),
            'vendedor_externo_colaborador_id' => rand(1, 20),
            'status'                          => $status[rand(1, 4)],
            'referencia_agencia'              => $faker->firstName,
            'referencia_banco'                => $faker->firstName,
            'referencia_conta'                => $faker->firstName,
            'referencia_data_abertura'        => $faker->date,
            'referencia_gerente'              => $faker->firstName,
            'referencia_obs'                  => $faker->sentence,
            'nascimento'                      => $faker->date,
            'setor_id'                        => rand(1, 20),
            'tabela_id'                       => 1,
            'usa_cartao'                      => (rand(0, 1)) ? true : false,
            'usa_cheque'                      => (rand(0, 1)) ? true : false,
            'obs'                             => $faker->sentence,
            'transportadora_id'               => rand(1, 20),
            'atividade_id'                    => 1,
            'pai'                             => $faker->name,
            'mae'                             => $faker->name,
            'site'                            => 'http://'.$faker->firstName.'.com',
            'prazo_id'                        => rand(1, 20),
            'consulta_data'                   => $faker->date,
            'preco_custo'                     => (rand(0, 1)) ? true : false,
            'clientes_rede_id'                => rand(1, 20),
            'clientes_grupo_id'               => rand(1, 20),
            'senha'                           => str_random(50),
            'regime'                          => (rand(0, 1)) ? 'Simples' : 'Normal',
            'sexo'                            => (rand(0, 1)) ? 'Masculino' : 'Feminino',
            'notificacao_sms'                 => (rand(0, 1)) ? true : false,
            'notificacao_email'               => (rand(0, 1)) ? true : false,
            'dia_fatura_semanal'              => $diaSemana[rand(1, 7)],
            'cobrar_taxa_boleto'              => (rand(0, 1)) ? true : false,
            'indice'                          => 1,
            'classificacao_id'                => rand(1, 20),
            'data_bloqueio'                   => $faker->date,
            'porte_id'                        => rand(1, 20),
            'limite_compra'                   => rand(10, 5000),
            'pode_ser_indicante'              => (rand(0, 1)) ? true : false,
    ];
});

$factory->define(Modules\ClienteRede\Entities\ClienteRede::class, function () use ($faker) {
    return [
            'nome' => $faker->firstName,
    ];
});

$factory->define(Modules\ClienteGrupo\Entities\ClienteGrupo::class, function () use ($faker) {
    return [
            'nome' => $faker->firstName,
    ];
});

$factory->define(Modules\Classificacao\Entities\Classificacao::class, function () use ($faker) {
    return [
            'nome' => $faker->firstName,
            'cor'  => 1,
    ];
});

$factory->define(Modules\Porte\Entities\Porte::class, function () use ($faker) {
    return [
            'nome'   => $faker->name,
            'fatura' => 1,
    ];
});

$factory->define(Modules\Fornecedor\Entities\Fornecedor::class, function () use ($faker) {
    return [
            'razao'             => $faker->name,
            'fantasia'          => $faker->firstName,
            'cnpj'              => str_random(14),
            'ie'                => 'isento',
            'endereco_id'       => rand(1, 20),
            'email'             => $faker->email,
            'telefone'          => '4141414141',
            'fax'               => '4141414141',
            'ramal'             => '10009',
            'site'              => 'http://'.$faker->firstName.'.com',
            'obs'               => $faker->sentence,
            'frete'             => (rand(0, 1)) ? 'CIF' : 'FOB',
            'transportadora_id' => rand(1, 20),
            'prazo_entrega'     => rand(1, 30),
            'prazo_id'          => rand(1, 20),
            'bonificacao'       => 1,
            'conta_bancaria_id' => rand(1, 20),
    ];
});

$factory->define(Modules\Grupo\Entities\Grupo::class, function () use ($faker) {
    return [
            'nome' => $faker->firstName,
    ];
});

$factory->define(Modules\Marca\Entities\Marca::class, function () use ($faker) {
    return [
            'nome' => $faker->firstName,
    ];
});

$factory->define(Modules\Ncm\Entities\Ncm::class, function () use ($faker) {
    return [
        'id'                 => '12345678',
        'titulo'             => $faker->name,
        'descricao'          => $faker->sentence,
        'aliquota_nacional'  => rand(1, 10000),
        'aliquota_importado' => rand(1, 10000),
        'aliquota_estadual'  => rand(1, 10000),
        'aliquota_municipal' => rand(1, 10000),
        'tabela'             => $faker->firstName,
    ];
});

$factory->define(Modules\Tratamento\Entities\Tratamento::class, function () use ($faker) {
    return [
            'nome' => $faker->firstName,
    ];
});

$factory->define(Modules\Segmento\Entities\Segmento::class, function () use ($faker) {
    return [
      'nome' => $faker->firstName,
      'obs'  => $faker->sentence,
      'obs2' => $faker->sentence,
    ];
});

$factory->define(Modules\ListaServico\Entities\ListaServico::class, function () use ($faker) {
    return [
        'cidades_ibge_verificador' => 1100015,
        'descricao'                => $faker->sentence,
        'aliquota_nacional'        => 1,
        'aliquota_importado'       => 1,
        'aliquota_estadual'        => 1,
        'aliquota_municipal'       => 1,
        'tabela'                   => str_random(10),
      ];
});

$factory->define(Modules\Cor\Entities\Cor::class, function () use ($faker) {
    return [
        'nome'  => $faker->firstName,
        'valor' => str_random(40),
        'tipo'  => $faker->randomElement($array = array('RGB', 'HEX')),
    ];
});

$factory->define(Modules\Cnae\Entities\Cnae::class, function () use ($faker) {
    return [
        'id'        => '1234567890',
        'descricao' => $faker->sentence,
    ];
});

$factory->define(Modules\Processo\Entities\Processo::class, function () use ($faker) {
    return [
        'nome'      => $faker->name,
        'descricao' => $faker->sentence,
    ];
});

$factory->define(Modules\Item\Entities\Item::class, function () use ($faker) {
    return [
        'codigo'                      => 1,
        'referencia'                  => str_random(20),
        'embalagem'                   => rand(1, 520),
        'unidade'                     => str_random(2),
        'nome'                        => $faker->name,
        'complemento'                 => $faker->firstName,
        'grupo_id'                    => rand(1, 20),
        'marca_id'                    => rand(1, 20),
        'barra_1'                     => str_random(15),
        'barra_2'                     => str_random(15),
        'barra_3'                     => str_random(15),
        'fornecedor_id'               => rand(1, 20),
        'ncm_id'                      => '12345678',
        'validade'                    => $faker->date,
        'tipo'                        => $faker->randomElement($array = array('Produto', 'Serviço', 'Insumo', 'Composto', 'Fabricado', 'Reparo', 'Locação')),
        'controla_estoque'            => (rand(0, 1)) ? true : false,
        'subgrupo_grupo_id'           => rand(1, 20),
        'descricao'                   => $faker->sentence,
        'segmento_id'                 => rand(1, 20),
        'destaque'                    => (rand(0, 1)) ? true : false,
        'cnae_id'                     => '1234567890',
        'lista_servico_id'            => rand(1, 20),
        'temporario'                  => (rand(0, 1)) ? true : false,
        'permitir_fracionar'          => (rand(0, 1)) ? true : false,
        'permitir_desconto'           => (rand(0, 1)) ? true : false,
        'permitir_abrir_embalagem'    => (rand(0, 1)) ? true : false,
        'permitir_comissao'           => (rand(0, 1)) ? true : false,
        'permitir_gerar_tabela'       => (rand(0, 1)) ? true : false,
        'permitir_selecionar_estoque' => (rand(0, 1)) ? true : false,
        'importado'                   => (rand(0, 1)) ? true : false,
        'web'                         => (rand(0, 1)) ? true : false,
        'mobile'                      => (rand(0, 1)) ? true : false,
        'ean_quantidade'              => 1,
        'bonificacao'                 => (rand(0, 1)) ? true : false,
        'frete_fixo'                  => (rand(0, 1)) ? true : false,
        'margem_fixa'                 => (rand(0, 1)) ? true : false,
        'caixa_com_ean'               => (rand(0, 1)) ? true : false,
        'produzido'                   => (rand(0, 1)) ? true : false,
        'permitir_producao'           => (rand(0, 1)) ? true : false,
        'nao_vender_sem_estoque'      => (rand(0, 1)) ? true : false,
        'status'                      => (rand(0, 1)) ? true : false,
     ];
});

$factory->define(Modules\ItemProcesso\Entities\ItemProcesso::class, function () use ($faker) {
    return [
            'nome'              => $faker->name,
            'item_id'           => rand(1, 20),
            'processo_id'       => rand(1, 20),
            'obs'               => $faker->sentence,
            'vinculado_item_id' => rand(1, 20),
            'ordem'             => rand(1, 10),
    ];
});

$factory->define(Modules\Royalty\Entities\Royalty::class, function () use ($faker) {

    return [
        'nome' => $faker->name,
    ];
});
