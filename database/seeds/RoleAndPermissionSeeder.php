<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class RoleAndPermissionSeeder extends Seeder
{
    public function run()
    {
        $this->roles();
        $this->permissions();
    }

    public function roles()
    {
        $data = json_decode(Storage::get('data/roles.json'));

        foreach ($data as $row) {
            $role = new Role();
            $role->fill((array) $row)->save();
        }
    }

    public function permissions()
    {
        $data = json_decode(Storage::get('data/permissions.json'));

        foreach ($data as $row) {
            $permission = new Permission();
            $permission->fill((array) $row)->save();

            // Attach
            $role = Role::first();
            $role->attachPermission($permission);
        }
    }
}
