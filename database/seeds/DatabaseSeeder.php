<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        // Permissões e Grupos
        // $this->call('RoleAndPermissionSeeder');

        // Paises / Estados / Cidades
        //$this->call('PaisTableSeeder');
        //$this->call('UfTableSeeder');
        //$this->call('CidadeTableSeeder');

        // Usuários
        //$this->call('ColaboradorTableSeeder');
        $this->call('UsuarioTableSeeder');

        // Permissions
        $this->call('PermissionTableSeeder');
    }
}
