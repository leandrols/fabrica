<?php

use Modules\Endereco\Entities\Uf;
use Illuminate\Database\Seeder;

class UfTableSeeder extends Seeder
{
    public function run()
    {
        $data = json_decode(file_get_contents('https://bitbucket.org/api/2.0/snippets/nerijunior/jgKAz/be8fbab0f1eba071919d196ab79111a2a3841520/files/ufs.json'));

        foreach ($data as $row) {
            $uf           = new Uf();
            $uf->nome     = $row->nome;
            $uf->regiao   = $row->regiao;
            $uf->aliquota = $row->aliquota;
            $uf->sigla    = $row->sigla;
            $uf->ibge     = $row->ibge;
            $uf->pais_id  = 1;
            $uf->save();
        }
    }
}
