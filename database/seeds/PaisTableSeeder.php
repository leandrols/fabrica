<?php

use Modules\Endereco\Entities\Pais;
use Illuminate\Database\Seeder;

class PaisTableSeeder extends Seeder
{
    public function run()
    {
        $pais = new Pais();
        //$pais->id          = 1;
        $pais->nome        = 'Brasil';
        $pais->complemento = 'Brasil';
        $pais->save();
    }
}
