<?php

use Illuminate\Database\Seeder;
use Artesaos\Defender\Permission;

class PermissionTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'name'          => 'colaborador.all',
                'readable_name' => 'Todos os colaboradores',
            ],
            [
                'name'          => 'colaborador.list',
                'readable_name' => 'Lista colaborador(es) pelo id',
            ],
            [
                'name'          => 'colaborador.create',
                'readable_name' => 'Adicionar colaborador',
            ],
            [
                'name'          => 'colaborador.search',
                'readable_name' => 'Pesquisar colaborador',
            ],
            [
                'name'          => 'colaborador.update',
                'readable_name' => 'Atualizar colaborador',
            ],
            [
                'name'          => 'colaborador.update',
                'readable_name' => 'Apagar colaborador',
            ],
            [
                'name'          => 'colaborador.restore',
                'readable_name' => 'Restaurar colaborador',
            ],
        ];
        $dates['created_at'] = date('Y-m-d H:i:s');
        $dates['updated_at'] = date('Y-m-d H:i:s');
        foreach ($permissions as $permission) {
            $per = permission::where('name', $permission['name'])->first();
            if (!isset($per['name'])) {
                $per = new permission();
                $per->insert(array_merge($permission, $dates));
            } else {
                $per->update(array_merge($permission, $dates));
            }
        }
    }
}
