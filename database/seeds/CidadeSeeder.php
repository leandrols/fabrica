<?php

use Modules\Endereco\Entities\Cidade;
use Illuminate\Database\Seeder;

class CidadeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = json_decode(file_get_contents('https://bitbucket.org/api/2.0/snippets/nerijunior/RoKL7/d88d8643c9abcd2d51aec49d78560cf3c3b2ed65/files/cidades.json'));

        foreach ($data as $row) {
            $cidade                   = new Cidade();
            $cidade->nome             = $row->nome;
            $cidade->cep              = $row->cep;
            $cidade->ibge             = $row->ibge;
            $cidade->ibge_verificador = $row->ibge_verificador;
            $cidade->uf_id            = $row->uf_id;

            $cidade->save();
        }
    }
}
