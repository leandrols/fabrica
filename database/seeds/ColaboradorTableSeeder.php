<?php

use Modules\Colaborador\Entities\Colaborador;
use Illuminate\Database\Seeder;

class ColaboradorTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'endereco_id'               => 1,
                'comissao_base'             => 'Nenhuma',
                'contas_bancaria_id'        => 1,
                'cpf'                       => '12345678901',
                'data_admissao'             => '2015-01-01',
                'data_nascimento'           => '2015-01-01',
                'dia_pagamento'             => '5',
                'caracteristicas_fisica_id' => 1,
                'horario_id'                => 1,
                'filial_id'                 => 1,
                'profissao_id'              => 1,
                'meta_positivacao'          => '',
                'nascimento_cidade_id'      => 1,
                'nome'                      => 'Neri Junior',
                'vic'                       => false,
            ],
        ];

        foreach ($data as $key => $row) {
            $user = Colaborador::create($row);

            // Role
            $user->attachRole(Role::first());
        }
    }
}
