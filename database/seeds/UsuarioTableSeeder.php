<?php

use Modules\Usuario\Entities\Usuario;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsuarioTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nome'           => 'Elias',
                'sobrenome'      => 'J',
                'login'          => 'elias',
                'email'          => 'elias@lstech.com.br',
                'senha'          => '123qwe',
                'colaborador_id' => 1,
            ],
            [
                'nome'           => 'Neri',
                'sobrenome'      => 'Junior',
                'login'          => 'nerijunior',
                'email'          => 'neri@nerijunior.com',
                'senha'          => '123qwe',
                'colaborador_id' => 1,
            ],
            [
                'nome'           => 'Leandro Henrique',
                'sobrenome'      => 'Reis',
                'login'          => 'leandro',
                'email'          => 'leandro@lstech.com.br',
                'senha'          => '123qwe',
                'colaborador_id' => 1,
            ],
        ];

        foreach ($data as $key => $row) {
            //$row['senha'] = Hash::make($row['senha']);
            $user = Usuario::create($row);

            // Role
            //$user->attachRole(Role::first());
        }
    }
}
