<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('codigo');
			$table->string('referencia', 20)->nullable();
			$table->float('embalagem', 10, 0)->default(1);
			$table->string('unidade', 2);
			$table->string('nome', 60);
			$table->string('complemento', 30)->nullable();
			$table->integer('grupo_id')->index('fk_itens_grupos1_idx');
			$table->integer('marca_id')->index('fk_itens_marcas1_idx');
			$table->string('barra_1', 15)->nullable();
			$table->string('barra_2', 16)->nullable();
			$table->string('barra_3', 16)->nullable();
			$table->text('obs', 65535)->nullable();
			$table->integer('fornecedor_id')->nullable()->index('fk_itens_fornecedores1_idx');
			$table->string('ncm_id', 8)->nullable()->index('fk_itens_ncms1_idx');
			$table->date('validade')->nullable();
			$table->enum('tipo', array('Produto','Serviço','Insumo','Composto','Fabricado','Reparo','Locação'));
			$table->boolean('controla_estoque')->default(1);
			$table->integer('tratamento_id')->nullable()->index('fk_itens_tratamentos1_idx');
			$table->float('valor_venda_minima', 10, 0)->nullable();
			$table->integer('subgrupo_grupo_id')->index('fk_itens_grupos2_idx');
			$table->string('descricao', 90);
			$table->float('peso_bruto', 10, 0)->nullable();
			$table->float('peso_liquido', 10, 0)->nullable();
			$table->integer('prazo_entrega')->nullable()->default(0);
			$table->integer('segmento_id')->index('fk_itens_segmentos1_idx');
			$table->string('altura', 45)->nullable();
			$table->string('largura', 45)->nullable();
			$table->string('profundidade', 45)->nullable();
			$table->text('obs_complementar', 65535)->nullable();
			$table->boolean('destaque')->default(0);
			$table->string('embalagem_pai', 45)->nullable();
			$table->string('cnae_id', 10)->index('fk_itens_cnaes1_idx');
			$table->integer('lista_servico_id')->index('fk_itens_lista_servicos1_idx');
			$table->boolean('temporario')->default(0);
			$table->float('desconto', 10, 0)->nullable();
			$table->boolean('permitir_fracionar')->default(0);
			$table->boolean('permitir_desconto')->default(1);
			$table->boolean('permitir_abrir_embalagem')->default(1);
			$table->boolean('permitir_comissao')->default(1);
			$table->boolean('permitir_gerar_tabela')->default(1);
			$table->integer('fator_embalagem')->nullable()->default(1);
			$table->boolean('permitir_selecionar_estoque')->default(0);
			$table->integer('cor_id')->nullable()->index('fk_itens_cores1_idx');
			$table->string('codigo_personalizado', 50)->nullable();
			$table->integer('codigo_anp')->nullable();
			$table->boolean('importado')->default(0);
			$table->float('margem_vic', 10, 0)->nullable();
			$table->boolean('web')->default(0);
			$table->boolean('mobile')->default(0);
			$table->float('ean_quantidade', 10, 0)->default(1);
			$table->boolean('bonificacao')->default(1);
			$table->boolean('frete_fixo')->default(0);
			$table->boolean('margem_fixa')->default(0);
			$table->string('contrato', 60)->nullable();
			$table->integer('responsavel_usuario_id')->unsigned()->nullable()->index('fk_itens_usuarios1_idx');
			$table->boolean('caixa_com_ean')->default(0);
			$table->integer('plano_conta_custo_id')->nullable()->index('fk_itens_plano_conta_custos1_idx');
			$table->boolean('produzido')->default(0);
			$table->integer('associado_item_id')->nullable()->index('fk_itens_itens1_idx');
			$table->boolean('permitir_producao')->default(1);
			$table->boolean('nao_vender_sem_estoque')->default(0);
			$table->string('descricao_etiqueta', 100)->nullable();
			$table->boolean('status')->default(1);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens');
	}

}
