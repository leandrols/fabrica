<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFornecedoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fornecedores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('razao', 50);
			$table->string('fantasia', 30);
			$table->string('cnpj', 14);
			$table->string('ie', 20);
			$table->integer('endereco_id')->index('fk_fornecedores_enderecos1_idx');
			$table->string('telefone', 12);
			$table->string('ramal', 5)->nullable();
			$table->string('representante', 30)->nullable();
			$table->string('email', 60)->nullable();
			$table->string('site', 60);
			$table->text('obs', 65535)->nullable();
			$table->string('fax', 12)->nullable();
			$table->string('celular', 20)->nullable();
			$table->enum('frete', array('CIF','FOB'))->default('FOB');
			$table->integer('transportadora_id')->nullable()->index('fk_fornecedores_transportadoras1_idx');
			$table->integer('prazo_entrega')->nullable();
			$table->integer('prazo_id')->index('fk_fornecedores_prazos1_idx');
			$table->float('bonificacao', 10, 0);
			$table->string('iest', 20)->nullable();
			$table->text('obs_nfe', 65535)->nullable();
			$table->string('exterior_uf', 30)->nullable();
			$table->string('exterior_telefone1', 30)->nullable();
			$table->string('exterior_telefone2', 30)->nullable();
			$table->integer('conta_bancaria_id')->index('fk_fornecedores_contas_bancarias1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fornecedores');
	}

}
