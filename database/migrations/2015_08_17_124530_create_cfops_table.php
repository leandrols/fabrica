<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCfopsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cfops', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('descricao', 50);
            $table->string('descricao_entrada', 45);
            $table->string('descricao_saida', 45);
            $table->integer('correspondente_saida_cfop_id')->nullable()->index('fk_cfops_cfops1_idx');
            $table->boolean('baixa_estoque')->default(1);
            $table->boolean('gera_icms')->default(1);
            $table->boolean('gera_pis')->default(1);
            $table->boolean('gera_cofins')->default(1);
            $table->enum('entrada_saida', array('Entrada', 'Saida', 'Ambos'))->default('Ambos');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('cfops');
    }
}
