<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 30);
			$table->string('valor', 40);
			$table->enum('tipo', array('RGB','HEX'))->default('HEX');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cores');
	}

}
