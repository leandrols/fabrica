<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUfsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ufs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 25);
			$table->string('sigla', 2);
			$table->string('regiao', 45);
			$table->float('aliquota', 10, 0)->nullable();
			$table->integer('ibge');
			$table->integer('pais_id')->index('fk_ufs_paises1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ufs');
	}

}
