<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensRestricoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_restricoes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_item_restricoes_itens1_idx');
			$table->integer('atividade_id')->index('fk_item_restricoes_atividades1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_restricoes');
	}

}
