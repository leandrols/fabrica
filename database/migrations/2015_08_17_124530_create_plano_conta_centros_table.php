<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanoContaCentrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plano_conta_centros', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plano_conta_categoria_id')->index('fk_plano_conta_centros_plano_conta_categorias1_idx');
			$table->string('classificacao', 4)->nullable();
			$table->boolean('mercadoria')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plano_conta_centros');
	}

}
