<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 50)->nullable();
			$table->string('host_entrada', 80);
			$table->integer('porta_entrada');
			$table->string('conta_entrada', 80);
			$table->string('email_entrada', 80);
			$table->string('senha_entrada', 80);
			$table->string('host_saida', 80);
			$table->integer('porta_saida');
			$table->string('conta_saida', 80);
			$table->string('email_saida', 80);
			$table->string('senha_saida', 80);
			$table->boolean('ssl')->default(1);
			$table->enum('protocolo', array('ssl','tsl'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emails');
	}

}
