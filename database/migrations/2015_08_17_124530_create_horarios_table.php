<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHorariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('horarios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->time('refeicao_inicio_dom')->nullable();
			$table->time('refeicao_inicio_seg')->nullable();
			$table->time('refeicao_inicio_ter')->nullable();
			$table->time('refeicao_inicio_qua')->nullable();
			$table->time('refeicao_inicio_qui')->nullable();
			$table->time('refeicao_inicio_sex')->nullable();
			$table->time('refeicao_inicio_sab')->nullable();
			$table->time('refeicao_fim_dom')->nullable();
			$table->time('refeicao_fim_seg')->nullable();
			$table->time('refeicao_fim_qua')->nullable();
			$table->time('refeicao_fim_qui')->nullable();
			$table->time('refeicao_fim_sab')->nullable();
			$table->time('refeicao_fim_sex')->nullable();
			$table->time('inicio_dom')->nullable();
			$table->time('inicio_qua')->nullable();
			$table->time('inicio_qui')->nullable();
			$table->time('inicio_sab')->nullable();
			$table->time('inicio_seg')->nullable();
			$table->time('inicio_sex')->nullable();
			$table->time('inicio_ter')->nullable();
			$table->time('fim_dom')->nullable();
			$table->time('fim_qua')->nullable();
			$table->time('fim_qui')->nullable();
			$table->time('fim_sab')->nullable();
			$table->time('fim_seg')->nullable();
			$table->time('fim_sex')->nullable();
			$table->time('fim_ter')->nullable();
			$table->time('intervalo_inicio_dom')->nullable();
			$table->time('intervalo_inicio_qua')->nullable();
			$table->time('intervalo_inicio_qui')->nullable();
			$table->time('intervalo_inicio_sab')->nullable();
			$table->time('intervalo_inicio_seg')->nullable();
			$table->time('intervalo_inicio_sex')->nullable();
			$table->time('intervalo_inicio_ter')->nullable();
			$table->time('intervalo_fim_dom')->nullable();
			$table->time('intervalo_fim_qua')->nullable();
			$table->time('intervalo_fim_qui')->nullable();
			$table->time('intervalo_fim_sab')->nullable();
			$table->time('intervalo_fim_seg')->nullable();
			$table->time('intervalo_fim_sex')->nullable();
			$table->time('intervalo_fim_ter')->nullable();
			$table->smallInteger('entrada_extra')->default(0);
			$table->smallInteger('tolerancia_entrada')->default(0);
			$table->smallInteger('tolerancia_refeicao')->default(0);
			$table->smallInteger('tolerancia_saida')->default(0);
			$table->boolean('banco_horas')->default(0);
			$table->boolean('hora_extra')->default(0);
			$table->smallInteger('extra_minimo')->nullable();
			$table->boolean('intervalo_automatico')->default(0);
			$table->time('refeicao_fim_ter')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('horarios');
	}

}
