<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensProcessosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_processos', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_itens_processos_itens1')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('processo_id', 'fk_itens_processos_processos')->references('id')->on('processos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vinculado_item_id', 'fk_itens_processos_itens2')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_processos', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_processos_itens1');
			$table->dropForeign('fk_itens_processos_processos');
			$table->dropForeign('fk_itens_processos_itens2');
		});
	}

}
