<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNcmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ncms', function(Blueprint $table)
		{
			$table->string('id', 8)->unique('idx_ncms_id');
			$table->string('titulo', 50);
			$table->text('descricao', 65535)->nullable();
			$table->float('margem_ipi', 10, 0)->nullable();
			$table->float('aliquota_nacional', 10, 0)->default(0);
			$table->float('aliquota_importado', 10, 0)->default(0);
			$table->float('aliquota_estadual', 10, 0)->default(0);
			$table->float('aliquota_municipal', 10, 0)->default(0);
			$table->string('tabela', 10)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ncms');
	}

}
