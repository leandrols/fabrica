<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColaboradoresTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('colaboradores', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('endereco_id')->index('fk_colaboradores_enderecos1_idx');
            $table->string('assinatura')->nullable();
            $table->boolean('bate_ponto')->nullable()->default(0);
            $table->string('celular', 11)->nullable();
            $table->boolean('comissionado')->default(0);
            $table->enum('comissao_base', array('Nenhuma', 'Desconto', 'Meta', 'Lucro'))->default('Nenhuma');
            $table->string('carteira_reservista', 20)->nullable();
            $table->string('carteira_reservista_categoria', 10)->nullable();
            $table->string('carteira_trabalho', 15)->nullable();
            $table->date('carteira_trabalho_data')->nullable();
            $table->string('carteira_trabalho_serie', 3)->nullable();
            $table->string('carteira_trabalho_uf', 3)->nullable();
            $table->integer('conta_bancaria_id')->index('fk_colaboradores_contas1_idx');
            $table->string('cpf', 11)->unique('cpf_UNIQUE');
            $table->date('data_admissao')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->smallInteger('dia_pagamento');
            $table->string('email', 60)->nullable();
            $table->text('email_ass', 65535)->nullable();
            $table->string('email_comercial', 60)->nullable();
            $table->integer('email_id')->index('fk_colaboradores_emails1_idx')->nullable();
            $table->string('escolaridade', 45)->nullable();
            $table->enum('estado_civil', array('Solteiro', 'Casado', 'Viuvo', 'Divorciado', 'Separado'))->nullable()->default('Solteiro');
            $table->string('foto', 100)->nullable();
            $table->integer('filhos')->nullable();
            $table->integer('caracteristicas_fisica_id')->index('fk_colaboradores_caracteristicas_fisicas1_idx');
            $table->integer('horario_id');
            $table->string('habilitacao', 45)->nullable();
            $table->enum('habilitacao_categoria', array('A', 'B', 'C', 'D', 'E', 'AB', 'AC', 'AD', 'AE'))->nullable();
            $table->integer('filial_id')->index('fk_colaboradores_filiais1_idx');
            $table->integer('profissao_id')->index('fk_colaboradores_profissoes1_idx');
            $table->integer('meta_positiva')->default(0);
            $table->float('meta_vendas', 10, 0)->nullable();
            $table->float('margem_comissao', 10, 0)->nullable();
            $table->float('multa_cancelamento', 10, 0)->nullable();
            $table->integer('nascimento_cidade_id')->index('fk_colaboradores_cidades1_idx');
            $table->string('nome', 50);
            $table->string('nome_conjuge', 50)->nullable();
            $table->string('nome_mae', 50)->nullable();
            $table->string('nome_pai', 50)->nullable();
            $table->text('obs', 65535)->nullable();
            $table->string('pis_numero', 15)->nullable();
            $table->date('pis_data')->nullable();
            $table->string('rg', 15)->nullable();
            $table->date('rg_data')->nullable();
            $table->string('rg_org_exp', 3)->nullable();
            $table->float('salario', 10, 0)->nullable();
            $table->enum('sexo', array('Masculino', 'Feminino'))->nullable();
            $table->boolean('smart_phone')->default(0);
            $table->boolean('status')->default(1);
            $table->string('telefone', 11)->nullable();
            $table->string('titulo_eleitor', 15)->nullable();
            $table->string('titulo_eleitor_secao', 3)->nullable();
            $table->string('titulo_eleitor_zona', 3)->nullable();
            $table->boolean('vic')->default(0);
            $table->integer('vic_colaborador_id')->nullable()->index('fk_colaboradores_colaboradores1_idx');
            $table->float('vic_limite', 10, 0)->default(0);
            $table->float('vic_margem', 10, 0)->default(0);
            $table->float('vic_saldo', 10, 0)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('colaboradores');
    }
}
