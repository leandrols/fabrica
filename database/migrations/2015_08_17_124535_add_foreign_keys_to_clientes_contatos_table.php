<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientesContatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clientes_contatos', function(Blueprint $table)
		{
			$table->foreign('contatos_id', 'fk_contatos_has_clientes_contatos1')->references('id')->on('contatos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('clientes_id', 'fk_contatos_has_clientes_clientes1')->references('id')->on('clientes')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clientes_contatos', function(Blueprint $table)
		{
			$table->dropForeign('fk_contatos_has_clientes_contatos1');
			$table->dropForeign('fk_contatos_has_clientes_clientes1');
		});
	}

}
