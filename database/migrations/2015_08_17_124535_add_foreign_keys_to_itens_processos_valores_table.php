<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensProcessosValoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_processos_valores', function(Blueprint $table)
		{
			$table->foreign('item_processo_id', 'fk_itens_processos_campos_valores_item_processos')->references('id')->on('itens_processos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('processo_campo_id', 'fk_itens_processos_campos_valores_processos_campos')->references('id')->on('processos_campos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_processos_valores', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_processos_campos_valores_item_processos');
			$table->dropForeign('fk_itens_processos_campos_valores_processos_campos');
		});
	}

}
