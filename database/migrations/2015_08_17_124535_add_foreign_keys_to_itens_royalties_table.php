<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensRoyaltiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_royalties', function(Blueprint $table)
		{
			$table->foreign('itens_id', 'fk_itens_royalties_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('royalty_id', 'fk_itens_royalties_royalties')->references('id')->on('royalties')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_royalties', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_royalties_itens');
			$table->dropForeign('fk_itens_royalties_royalties');
		});
	}

}
