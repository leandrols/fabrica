<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContasBancariasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contas_bancarias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 25);
			$table->string('banco', 4);
			$table->string('agencia', 5);
			$table->string('conta', 15);
			$table->string('favorecido', 45);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contas_bancarias');
	}

}
