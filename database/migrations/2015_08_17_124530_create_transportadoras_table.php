<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransportadorasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transportadoras', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('razao', 50);
			$table->string('fantasia', 30);
			$table->string('cnpj', 14);
			$table->string('ie', 20);
			$table->integer('endereco_id')->index('fk_transportadoras_enderecos1_idx');
			$table->string('telefone', 11);
			$table->string('email', 60)->nullable();
			$table->string('site', 60)->nullable();
			$table->text('obs', 65535)->nullable();
			$table->integer('matriz_transportadora_id')->nullable()->index('fk_transportadoras_transportadoras1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transportadoras');
	}

}
