<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanosContasCustosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('planos_contas_custos', function(Blueprint $table)
		{
			$table->foreign('plano_conta_centro_id', 'fk_planos_contas_custos_planos_contas_centros')->references('id')->on('plano_conta_centros')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('planos_contas_custos', function(Blueprint $table)
		{
			$table->dropForeign('fk_planos_contas_custos_planos_contas_centros');
		});
	}

}
