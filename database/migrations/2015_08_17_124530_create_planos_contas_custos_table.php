<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanosContasCustosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planos_contas_custos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 40)->nullable();
			$table->integer('plano_conta_centro_id')->index('fk_plano_conta_custos_plano_conta_centros1_idx');
			$table->float('valor_ideal', 10, 0)->nullable();
			$table->string('conta_contabil', 20)->nullable();
			$table->string('classificacao', 4)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planos_contas_custos');
	}

}
