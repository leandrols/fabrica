<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensHistoricosEstoquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_historicos_estoques', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_item_historicos_itens1_idx');
			$table->dateTime('data');
			$table->string('msg');
			$table->integer('usuario_id')->unsigned()->index('fk_item_historicos_usuarios1_idx');
			$table->float('custo', 10, 0);
			$table->float('estoque', 10, 0);
			$table->float('alteracao', 10, 0);
			$table->integer('filial_id')->index('fk_item_historicos_filiais1_idx');
			$table->string('razao', 50)->nullable();
			$table->integer('registro_id')->default(0);
			$table->string('tipo_registro', 10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_historicos_estoques');
	}

}
