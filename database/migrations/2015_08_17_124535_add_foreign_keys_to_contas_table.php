<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contas', function(Blueprint $table)
		{
			$table->foreign('filial_id', 'fk_contas_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('conta_bancaria_id', 'fk_contas_contas_bancarias')->references('id')->on('contas_bancarias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contas', function(Blueprint $table)
		{
			$table->dropForeign('fk_contas_filiais');
			$table->dropForeign('fk_contas_contas_bancarias');
		});
	}

}
