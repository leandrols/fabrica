<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToColaboradoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('colaboradores', function(Blueprint $table)
		{
			$table->foreign('endereco_id', 'fk_colaboradores_enderecos')->references('id')->on('enderecos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('conta_bancaria_id', 'fk_colaboradores_contas')->references('id')->on('contas_bancarias')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('email_id', 'fk_colaboradores_emails')->references('id')->on('emails')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('caracteristicas_fisica_id', 'fk_colaboradores_caracteristicas_fisicas')->references('id')->on('caracteristicas_fisicas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id', 'fk_colaboradores_horarios')->references('id')->on('horarios')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_colaboradores_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('profissao_id', 'fk_colaboradores_profissoes')->references('id')->on('profissoes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('nascimento_cidade_id', 'fk_colaboradores_cidades')->references('id')->on('cidades')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vic_colaborador_id', 'fk_colaboradores_colaboradores')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('colaboradores', function(Blueprint $table)
		{
			$table->dropForeign('fk_colaboradores_enderecos');
			$table->dropForeign('fk_colaboradores_contas');
			$table->dropForeign('fk_colaboradores_emails');
			$table->dropForeign('fk_colaboradores_caracteristicas_fisicas');
			$table->dropForeign('fk_colaboradores_horarios');
			$table->dropForeign('fk_colaboradores_filiais');
			$table->dropForeign('fk_colaboradores_profissoes');
			$table->dropForeign('fk_colaboradores_cidades');
			$table->dropForeign('fk_colaboradores_colaboradores');
		});
	}

}
