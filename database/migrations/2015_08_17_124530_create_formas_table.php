<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('formas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('conta_id')->index('fk_formas_contas1_idx');
			$table->integer('despesa_plano_conta_custo_id')->nullable()->index('fk_formas_plano_conta_custos1_idx');
			$table->string('nome', 20)->nullable();
			$table->integer('carencia')->default(0);
			$table->float('mora', 10, 0)->default(0);
			$table->integer('ordem')->default(0);
			$table->float('despesa_margem', 10, 0)->default(0);
			$table->float('despeza_valor', 10, 0)->default(0);
			$table->float('valor_minimo', 10, 0)->default(0);
			$table->boolean('smart_phone')->default(1);
			$table->boolean('tef')->default(0);
			$table->enum('tabela', array('Todas','Receber','Pagar'))->default('Todas');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('formas');
	}

}
