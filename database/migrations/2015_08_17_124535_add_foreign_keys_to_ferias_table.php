<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ferias', function(Blueprint $table)
		{
			$table->foreign('colaboradores_id', 'fk_ferias_colaboradores1')->references('id')->on('colaboradores')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ferias', function(Blueprint $table)
		{
			$table->dropForeign('fk_ferias_colaboradores1');
		});
	}

}
