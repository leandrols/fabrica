<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('razao', 60)->nullable();
			$table->string('fantasia', 60)->nullable();
			$table->string('cnpj', 14)->nullable();
			$table->string('ie', 20)->nullable();
			$table->integer('endereco_id')->index('fk_clientes_enderecos1_idx');
			$table->integer('cobranca_endereco_id')->nullable()->index('fk_clientes_enderecos2_idx');
			$table->string('email', 60)->nullable();
			$table->string('telefone', 11);
			$table->string('ramal', 5)->nullable();
			$table->string('telefone2', 11)->nullable();
			$table->string('fax', 11)->nullable();
			$table->integer('forma_id')->index('fk_clientes_formas1_idx');
			$table->integer('vendedor_colaborador_id')->index('fk_clientes_colaboradores1_idx');
			$table->integer('vendedor_externo_colaborador_id')->index('fk_clientes_colaboradores2_idx');
			$table->enum('status', array('Ativo','Inativo','Bloqueado','A vista'))->default('Ativo');
			$table->string('referencia_agencia', 20)->nullable();
			$table->string('referencia_banco', 20)->nullable();
			$table->string('referencia_conta', 20)->nullable();
			$table->date('referencia_data_abertura')->nullable();
			$table->string('referencia_gerente', 20)->nullable();
			$table->string('referencia_telefone_gerente', 11)->nullable();
			$table->text('referencia_obs', 65535)->nullable();
			$table->date('nascimento')->nullable();
			$table->integer('setor_id')->index('fk_clientes_setores1_idx');
			$table->integer('tabela_id')->index('fk_clientes_tabelas1_idx');
			$table->string('iss', 14)->nullable();
			$table->boolean('usa_cartao')->default(0);
			$table->string('bandeira_cartao', 30)->nullable();
			$table->boolean('usa_cheque')->default(0);
			$table->string('titulo_eleitor', 30)->nullable();
			$table->string('habilitacao', 30)->nullable();
			$table->string('rg', 30)->nullable();
			$table->text('obs', 65535)->nullable();
			$table->float('desconto', 10, 0)->default(0);
			$table->float('limite', 10, 0)->default(0);
			$table->integer('transportadora_id')->nullable()->index('fk_clientes_transportadoras1_idx');
			$table->integer('atividade_id')->index('fk_clientes_atividades1_idx');
			$table->string('pai', 60)->nullable();
			$table->string('mae', 60)->nullable();
			$table->string('site', 45)->nullable();
			$table->integer('prazo_id')->index('fk_clientes_prazos1_idx');
			$table->date('consulta_data')->nullable();
			$table->string('consulta_obs', 80)->nullable();
			$table->string('motivo', 80)->nullable();
			$table->boolean('preco_custo')->default(0);
			$table->string('exterior_uf', 30)->nullable();
			$table->string('exterior_telefone1', 30)->nullable();
			$table->string('exterior_telefone2', 30)->nullable();
			$table->string('suframa', 9)->nullable();
			$table->float('margem_comissao', 10, 0)->nullable();
			$table->string('iest', 20)->nullable();
			$table->integer('clientes_rede_id')->index('fk_clientes_clientes_redes1_idx');
			$table->integer('clientes_grupo_id')->index('fk_clientes_clientes_grupos1_idx');
			$table->string('senha', 50)->nullable();
			$table->text('consignatario', 65535)->nullable();
			$table->text('notify', 65535)->nullable();
			$table->enum('regime', array('Simples','Normal'))->default('Normal');
			$table->enum('sexo', array('Masculino','Feminino'))->nullable();
			$table->boolean('notificacao_sms')->default(0);
			$table->boolean('notificacao_email')->default(0);
			$table->string('celular', 11)->nullable();
			$table->smallInteger('dia_fatura1')->nullable();
			$table->smallInteger('dia_fatura2')->nullable();
			$table->smallInteger('dia_fatura3')->nullable();
			$table->enum('dia_fatura_semanal', array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'))->nullable();
			$table->integer('indicante_cliente_id')->nullable()->index('fk_clientes_clientes1_idx');
			$table->boolean('cobrar_taxa_boleto')->default(0);
			$table->float('indice', 10, 0);
			$table->integer('classificacao_id')->index('fk_clientes_classificacao1_idx');
			$table->date('data_bloqueio')->nullable();
			$table->float('desconto_colaborador', 10, 0)->default(0);
			$table->integer('porte_id')->index('fk_clientes_portes1_idx');
			$table->string('cartao_fidelidade', 32)->nullable();
			$table->string('responsavel', 100)->nullable();
			$table->smallInteger('dia_limite_fatura')->nullable();
			$table->float('limite_compra', 10, 0)->nullable();
			$table->boolean('pode_ser_indicante')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
