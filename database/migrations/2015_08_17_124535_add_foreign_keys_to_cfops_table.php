<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCfopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cfops', function(Blueprint $table)
		{
			$table->foreign('correspondente_saida_cfop_id', 'fk_cfops_cfops1')->references('id')->on('cfops')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cfops', function(Blueprint $table)
		{
			$table->dropForeign('fk_cfops_cfops1');
		});
	}

}
