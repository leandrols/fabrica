<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFormasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('formas', function(Blueprint $table)
		{
			$table->foreign('despesa_plano_conta_custo_id', 'fk_formas_plano_conta_custos')->references('id')->on('planos_contas_custos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('conta_id', 'fk_formas_contas')->references('id')->on('contas')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('formas', function(Blueprint $table)
		{
			$table->dropForeign('fk_formas_plano_conta_custos');
			$table->dropForeign('fk_formas_contas');
		});
	}

}
