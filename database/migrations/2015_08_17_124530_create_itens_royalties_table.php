<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensRoyaltiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_royalties', function(Blueprint $table)
		{
			$table->integer('itens_id')->index('fk_item_royalties_itens1_idx');
			$table->integer('royalty_id')->index('fk_item_royalties_royalties1_idx');
			$table->float('valor', 10, 0)->nullable();
			$table->float('percentual', 10, 0)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_royalties');
	}

}
