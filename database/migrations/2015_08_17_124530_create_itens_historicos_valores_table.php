<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensHistoricosValoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_historicos_valores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('filial_id')->index('fk_item_historico_valores_filiais1_idx');
			$table->integer('item_id')->index('fk_item_historico_valores_itens1_idx');
			$table->dateTime('data');
			$table->integer('usuario_id')->unsigned()->index('fk_item_historico_valores_usuarios1_idx');
			$table->string('msg', 100);
			$table->string('campo', 50);
			$table->string('tela', 50);
			$table->string('valor_old', 100);
			$table->string('valor_new', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_historicos_valores');
	}

}
