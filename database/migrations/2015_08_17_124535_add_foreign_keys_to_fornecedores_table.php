<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFornecedoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fornecedores', function(Blueprint $table)
		{
			$table->foreign('endereco_id', 'fk_fornecedores_enderecos')->references('id')->on('enderecos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('transportadora_id', 'fk_fornecedores_transportadoras')->references('id')->on('transportadoras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('prazo_id', 'fk_fornecedores_prazos')->references('id')->on('prazos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('conta_bancaria_id', 'fk_fornecedores_contas_bancarias')->references('id')->on('contas_bancarias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fornecedores', function(Blueprint $table)
		{
			$table->dropForeign('fk_fornecedores_enderecos');
			$table->dropForeign('fk_fornecedores_transportadoras');
			$table->dropForeign('fk_fornecedores_prazos');
			$table->dropForeign('fk_fornecedores_contas_bancarias');
		});
	}

}
