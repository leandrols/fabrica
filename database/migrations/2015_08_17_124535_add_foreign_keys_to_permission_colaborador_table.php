<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPermissionColaboradorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permission_colaborador', function(Blueprint $table)
		{
			$table->foreign('colaboradores_id', 'fk_colaboradores_has_permissions_colaboradores')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('permissions_id', 'fk_colaboradores_has_permissions_permissions')->references('id')->on('permissions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permission_colaborador', function(Blueprint $table)
		{
			$table->dropForeign('fk_colaboradores_has_permissions_colaboradores');
			$table->dropForeign('fk_colaboradores_has_permissions_permissions');
		});
	}

}
