<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pedidos', function(Blueprint $table)
		{
			$table->foreign('cliente_id', 'fk_pedidos_clientes_id')->references('id')->on('clientes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vendedor_colaborador_id', 'fk_pedidos_vendedor_colaboradores_id')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('entregador_colaborador_id', 'fk_pedidos_entregador_colaboradores_id')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('separador_colaborador_id', 'fk_pedidos_separador_colaboradores_id')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('conferente_colaborador_id', 'fk_pedidos_conferente_colaboradores_id')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('atendente_colaborador_id', 'fk_pedidos_atendente_colaboradores_id')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vendedor_externo_colaborador_id', 'fk_pedidos_vendedor_externo_colaboradores_id')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('tipo_realizacao_id', 'fk_pedidos_tipo_realizacoes')->references('id')->on('tipos_realizacoes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('forma_id', 'fk_pedidos_formas')->references('id')->on('formas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('prazo_id', 'fk_pedidos_prazos')->references('id')->on('prazos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_pedidos_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('indicante_cliente_id', 'fk_pedidos_indicante_clientes_id')->references('id')->on('clientes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('transportadora_id', 'fk_pedidos_transportadoras')->references('id')->on('transportadoras')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('pai_pedido_id', 'fk_pedidos_pedidos')->references('id')->on('pedidos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('ordem_producao_id', 'fk_pedidos_ordens_producoes')->references('id')->on('ordens_producoes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('ordem_servico_id', 'fk_pedidos_ordens_servicos')->references('id')->on('ordens_servicos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('lista_entrega_id', 'fk_pedidos_listas_entregas')->references('id')->on('lista_entregas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cliente_rede_id', 'fk_pedidos_clientes_redes')->references('id')->on('clientes_redes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('periodo_entrega_id', 'fk_pedidos_periodos_entregas')->references('id')->on('periodos_entregas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cfop_id', 'fk_pedidos_cfops1')->references('id')->on('cfops')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('endereco_id', 'fk_pedidos_enderecos1')->references('id')->on('enderecos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('projetos_id', 'fk_pedidos_projetos1')->references('id')->on('projetos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pedidos', function(Blueprint $table)
		{
			$table->dropForeign('fk_pedidos_clientes_id');
			$table->dropForeign('fk_pedidos_vendedor_colaboradores_id');
			$table->dropForeign('fk_pedidos_entregador_colaboradores_id');
			$table->dropForeign('fk_pedidos_separador_colaboradores_id');
			$table->dropForeign('fk_pedidos_conferente_colaboradores_id');
			$table->dropForeign('fk_pedidos_atendente_colaboradores_id');
			$table->dropForeign('fk_pedidos_vendedor_externo_colaboradores_id');
			$table->dropForeign('fk_pedidos_tipo_realizacoes');
			$table->dropForeign('fk_pedidos_formas');
			$table->dropForeign('fk_pedidos_prazos');
			$table->dropForeign('fk_pedidos_filiais');
			$table->dropForeign('fk_pedidos_indicante_clientes_id');
			$table->dropForeign('fk_pedidos_transportadoras');
			$table->dropForeign('fk_pedidos_pedidos');
			$table->dropForeign('fk_pedidos_ordens_producoes');
			$table->dropForeign('fk_pedidos_ordens_servicos');
			$table->dropForeign('fk_pedidos_listas_entregas');
			$table->dropForeign('fk_pedidos_clientes_redes');
			$table->dropForeign('fk_pedidos_periodos_entregas');
			$table->dropForeign('fk_pedidos_cfops1');
			$table->dropForeign('fk_pedidos_enderecos1');
			$table->dropForeign('fk_pedidos_projetos1');
		});
	}

}
