<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaracteristicasFisicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('caracteristicas_fisicas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('altura', 10, 0)->nullable();
			$table->string('cabelo', 15)->nullable();
			$table->boolean('deficiente')->default(0);
			$table->string('olhos', 15)->nullable();
			$table->float('peso', 10, 0)->nullable();
			$table->enum('raca', array('Caucasiano','Negro','Pardo','Indígena','Amarelo'))->nullable();
			$table->string('sangue', 3)->nullable();
			$table->boolean('rh')->nullable();
			$table->string('sinais', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('caracteristicas_fisicas');
	}

}
