<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensEstoquesTabelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_estoques_tabelas', function(Blueprint $table)
		{
			$table->integer('tabelas_id')->index('fk_tabelas_has_item_estoques_tabelas1_idx');
			$table->integer('item_estoque_id')->index('fk_tabelas_has_item_estoques_item_estoques1_idx');
			$table->float('valor', 10, 0);
			$table->primary(['tabelas_id','item_estoque_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_estoques_tabelas');
	}

}
