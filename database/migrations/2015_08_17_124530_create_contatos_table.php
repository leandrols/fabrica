<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contatos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 50);
			$table->string('telefone', 11)->nullable();
			$table->string('celular', 11)->nullable();
			$table->string('email', 60)->nullable();
			$table->date('nascimento')->nullable();
			$table->string('cargo', 20)->nullable();
			$table->string('nome_cordial', 50)->nullable();
			$table->integer('endereco_id')->nullable()->index('fk_contatos_enderecos1_idx');
			$table->enum('sexo', array('Masculino','Feminino'))->nullable();
			$table->boolean('enviar_boleto')->default(0);
			$table->boolean('enviar_nfe')->default(0);
			$table->boolean('enviar_email')->default(0);
			$table->boolean('enivar_sms')->default(0);
			$table->boolean('oculto')->default(0);
			$table->string('senha', 45)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contatos');
	}

}
