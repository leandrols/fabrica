<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCidadesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('cidades', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nome', 50);
            $table->string('cep', 8);
            $table->integer('ibge');
            $table->integer('ibge_verificador')->unique('idx_cidades_ibge_verificador');
            $table->integer('uf_id')->index('fk_cidades_ufs1_idx');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('cidades');
    }
}
