<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensEstoquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_estoques', function(Blueprint $table)
		{
			$table->foreign('itens_id', 'fk_itens_estoques_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_itens_estoques_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_estoques', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_estoques_itens');
			$table->dropForeign('fk_itens_estoques_filiais');
		});
	}

}
