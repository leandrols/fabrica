<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensLocacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_locacoes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_item_locacoes_itens1_idx');
			$table->integer('filial_id')->index('fk_item_locacoes_filiais1_idx');
			$table->integer('usuario_id')->unsigned()->index('fk_item_locacoes_usuarios1_idx');
			$table->string('identificador', 45);
			$table->integer('nf_numero')->nullable();
			$table->date('data_entrada');
			$table->boolean('locado')->default(0);
			$table->integer('pedido_id')->index('fk_item_locacoes_pedidos1_idx');
			$table->softDeletes();
			$table->string('qrcode', 60)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_locacoes');
	}

}
