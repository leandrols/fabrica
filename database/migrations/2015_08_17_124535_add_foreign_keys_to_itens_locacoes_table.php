<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensLocacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_locacoes', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_itens_locacoes_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_itens_locacoes_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('usuario_id', 'fk_itens_locacoes_usuarios')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('pedido_id', 'fk_itens_locacoes_pedidos')->references('id')->on('pedidos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_locacoes', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_locacoes_itens');
			$table->dropForeign('fk_itens_locacoes_filiais');
			$table->dropForeign('fk_itens_locacoes_usuarios');
			$table->dropForeign('fk_itens_locacoes_pedidos');
		});
	}

}
