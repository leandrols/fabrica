<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensPerguntasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_perguntas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_timestamp_itens1_idx');
			$table->string('titulo', 100);
			$table->string('titulo_descricao', 300)->nullable();
			$table->string('pergunta', 500);
			$table->string('pergunta_descricao', 200)->nullable();
			$table->enum('campo', array('Pequeno','Grande'));
			$table->string('info', 500)->nullable();
			$table->integer('ordem')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_perguntas');
	}

}
