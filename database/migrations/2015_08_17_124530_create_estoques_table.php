<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstoquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estoques', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_estoques_itens1_idx');
			$table->integer('filial_id')->index('fk_estoques_filiais1_idx');
			$table->integer('fornecedor_id')->nullable()->index('fk_estoques_fornecedores1_idx');
			$table->string('lote', 30)->nullable();
			$table->float('quantidade', 10, 0);
			$table->float('saldo', 10, 0);
			$table->date('data_entrada');
			$table->integer('nf_entrada')->nullable();
			$table->float('custo', 10, 0);
			$table->date('validade')->nullable();
			$table->string('fci', 36)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estoques');
	}

}
