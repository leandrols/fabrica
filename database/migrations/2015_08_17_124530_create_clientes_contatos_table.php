<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesContatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes_contatos', function(Blueprint $table)
		{
			$table->integer('clientes_id')->index('fk_contatos_has_clientes_clientes1_idx');
			$table->integer('contatos_id')->index('fk_contatos_has_clientes_contatos1_idx');
			$table->primary(['clientes_id','contatos_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes_contatos');
	}

}
