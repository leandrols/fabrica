<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensPerguntasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_perguntas', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_itens_perguntas_itens_id')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_perguntas', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_perguntas_itens_id');
		});
	}

}
