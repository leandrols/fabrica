<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVolumesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volumes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pedidos_id')->index('fk_volumes_pedidos1_idx');
			$table->integer('uf_id')->index('fk_volumes_ufs1_idx');
			$table->string('numero', 10)->nullable();
			$table->integer('quantidade');
			$table->string('especie', 10);
			$table->string('marca', 20)->nullable();
			$table->string('placa', 8)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volumes');
	}

}
