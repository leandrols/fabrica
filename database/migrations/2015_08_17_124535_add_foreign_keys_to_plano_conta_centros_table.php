<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanoContaCentrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plano_conta_centros', function(Blueprint $table)
		{
			$table->foreign('plano_conta_categoria_id', 'fk_planos_contas_centros_planos_contas_categorias')->references('id')->on('planos_contas_categorias')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plano_conta_centros', function(Blueprint $table)
		{
			$table->dropForeign('fk_planos_contas_centros_planos_contas_categorias');
		});
	}

}
