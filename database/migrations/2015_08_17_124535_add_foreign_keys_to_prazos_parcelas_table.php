<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPrazosParcelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('prazos_parcelas', function(Blueprint $table)
		{
			$table->foreign('prazo_id', 'fk_prazos_parcelas_prazos')->references('id')->on('prazos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('prazos_parcelas', function(Blueprint $table)
		{
			$table->dropForeign('fk_prazos_parcelas_prazos');
		});
	}

}
