<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrazosParcelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prazos_parcelas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('prazo_id')->index('fk_prazos_parcelas_prazos1_idx');
			$table->integer('dias')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prazos_parcelas');
	}

}
