<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensEstoquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_estoques', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('itens_id')->index('fk_item_estoques_itens1_idx');
			$table->float('estoque_atual', 10, 0);
			$table->float('custo_atual', 10, 0)->nullable();
			$table->float('custo_medio', 10, 0)->nullable();
			$table->float('reserva', 10, 0)->nullable();
			$table->integer('filial_id')->index('fk_item_estoques_filiais1_idx');
			$table->float('servico_inss', 10, 0)->nullable();
			$table->float('servico_iss', 10, 0)->nullable();
			$table->float('servico_ir', 10, 0)->nullable();
			$table->float('servico_csll', 10, 0)->nullable();
			$table->float('margem_cpp', 10, 0)->nullable();
			$table->float('valor_custo', 10, 0)->nullable();
			$table->float('margem_desconto_1', 10, 0)->nullable();
			$table->float('margem_desconto_2', 10, 0)->nullable();
			$table->float('margem_desconto_3', 10, 0)->nullable();
			$table->float('margem_desconto_4', 10, 0)->nullable();
			$table->float('margem_desconto_5', 10, 0)->nullable();
			$table->float('margem_ipi', 10, 0)->nullable();
			$table->float('margem_st', 10, 0)->nullable();
			$table->float('margem_icms', 10, 0)->nullable();
			$table->float('margem_icms_dif', 10, 0)->nullable();
			$table->float('margem_base_icms_dif', 10, 0)->nullable();
			$table->float('margem_mva', 10, 0)->nullable();
			$table->float('margem_mva_dif', 10, 0)->nullable();
			$table->float('margem_icms_st', 10, 0)->nullable();
			$table->float('margem_pis', 10, 0)->nullable();
			$table->float('margem_cofin', 10, 0)->nullable();
			$table->float('cst_ipi', 10, 0)->nullable();
			$table->float('cst_pis', 10, 0)->nullable();
			$table->float('cst_cofins', 10, 0)->nullable();
			$table->float('cst_icms', 10, 0)->nullable();
			$table->float('markup_divisor_1', 10, 0)->nullable();
			$table->float('markup_divisor_2', 10, 0)->nullable();
			$table->float('markup_divisor_3', 10, 0)->nullable();
			$table->float('markup_divisor_4', 10, 0)->nullable();
			$table->float('markup_divisor_5', 10, 0)->nullable();
			$table->float('markup_multiplicador_1', 10, 0)->nullable();
			$table->float('markup_multiplicador_2', 10, 0)->nullable();
			$table->float('markup_multiplicador_3', 10, 0)->nullable();
			$table->float('markup_multiplicador_4', 10, 0)->nullable();
			$table->float('markup_multiplicador_5', 10, 0)->nullable();
			$table->float('margem_minima_1', 10, 0)->nullable();
			$table->float('margem_minima_2', 10, 0)->nullable();
			$table->float('margem_minima_3', 10, 0)->nullable();
			$table->float('margem_minima_4', 10, 0)->nullable();
			$table->float('margem_minima_5', 10, 0)->nullable();
			$table->float('margem_comissao_1', 10, 0)->nullable();
			$table->float('margem_comissao_2', 10, 0)->nullable();
			$table->float('margem_comissao_3', 10, 0)->nullable();
			$table->float('margem_comissao_4', 10, 0)->nullable();
			$table->float('margem_comissao_5', 10, 0)->nullable();
			$table->float('markup_dividor_old_1', 10, 0)->nullable();
			$table->float('markup_dividor_old_2', 10, 0)->nullable();
			$table->float('markup_dividor_old_3', 10, 0)->nullable();
			$table->float('markup_dividor_old_4', 10, 0)->nullable();
			$table->float('markup_dividor_old_5', 10, 0)->nullable();
			$table->float('margem_frete', 10, 0)->nullable();
			$table->float('valor_frete', 10, 0)->nullable();
			$table->float('valor_producao', 10, 0)->nullable();
			$table->float('margem_indice', 10, 0)->nullable();
			$table->float('estoque_minimo', 10, 0)->nullable();
			$table->float('estoque_maximo', 10, 0)->nullable();
			$table->string('estoque_endereco', 20)->nullable();
			$table->float('margem_dif_bst', 10, 0)->nullable();
			$table->dateTime('data_limite_promocao')->nullable();
			$table->date('data_entrada')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_estoques');
	}

}
