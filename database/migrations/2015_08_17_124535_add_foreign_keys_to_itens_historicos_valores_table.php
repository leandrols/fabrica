<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensHistoricosValoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_historicos_valores', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_itens_historico_valores_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_itens_historico_valores_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('usuario_id', 'fk_itens_historico_valores_usuarios')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_historicos_valores', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_historico_valores_itens');
			$table->dropForeign('fk_itens_historico_valores_filiais');
			$table->dropForeign('fk_itens_historico_valores_usuarios');
		});
	}

}
