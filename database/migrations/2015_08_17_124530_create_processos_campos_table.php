<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessosCamposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('processos_campos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 20);
			$table->integer('processo_id')->index('fk_processo_campos_processos1_idx');
			$table->enum('tipo', array('Checkbox','Input'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('processos_campos');
	}

}
