<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTransportadorasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transportadoras', function(Blueprint $table)
		{
			$table->foreign('endereco_id', 'fk_transportadoras_enderecos')->references('id')->on('enderecos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('matriz_transportadora_id', 'fk_transportadoras_transportadoras')->references('id')->on('transportadoras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transportadoras', function(Blueprint $table)
		{
			$table->dropForeign('fk_transportadoras_enderecos');
			$table->dropForeign('fk_transportadoras_transportadoras');
		});
	}

}
