<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensEstoquesTabelasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_estoques_tabelas', function(Blueprint $table)
		{
			$table->foreign('tabelas_id', 'fk_tabelas_has_itens_estoques_tabelas')->references('id')->on('tabelas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('item_estoque_id', 'fk_tabelas_has_itens_estoques_itens_estoques')->references('id')->on('itens_estoques')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_estoques_tabelas', function(Blueprint $table)
		{
			$table->dropForeign('fk_tabelas_has_itens_estoques_tabelas');
			$table->dropForeign('fk_tabelas_has_itens_estoques_itens_estoques');
		});
	}

}
