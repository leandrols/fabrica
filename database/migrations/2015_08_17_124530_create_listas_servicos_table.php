<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateListasServicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listas_servicos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cidades_ibge_verificador')->index('idx_lista_servicos_cidades_ibge_verificador');
			$table->string('descricao', 500);
			$table->float('aliquota_nacional', 10, 0)->default(0);
			$table->float('aliquota_importado', 10, 0)->default(0);
			$table->float('aliquota_estadual', 10, 0)->default(0);
			$table->float('aliquota_municipal', 10, 0)->default(0);
			$table->string('tabela', 10)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listas_servicos');
	}

}
