<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens', function(Blueprint $table)
		{
			$table->foreign('grupo_id', 'fk_itens_grupos1')->references('id')->on('grupos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('marca_id', 'fk_itens_marcas')->references('id')->on('marcas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('fornecedor_id', 'fk_itens_fornecedores')->references('id')->on('fornecedores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('ncm_id', 'fk_itens_ncms')->references('id')->on('ncms')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('tratamento_id', 'fk_itens_tratamentos')->references('id')->on('tratamentos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('subgrupo_grupo_id', 'fk_itens_grupos2')->references('id')->on('grupos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('segmento_id', 'fk_itens_segmentos')->references('id')->on('segmentos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cnae_id', 'fk_itens_cnaes')->references('id')->on('cnaes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cor_id', 'fk_itens_cores')->references('id')->on('cores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('responsavel_usuario_id', 'fk_itens_usuarios')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('plano_conta_custo_id', 'fk_itens_planos_contas_custos')->references('id')->on('planos_contas_custos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('associado_item_id', 'fk_itens_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('lista_servico_id', 'fk_itens_lista_servicos')->references('id')->on('listas_servicos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_grupos1');
			$table->dropForeign('fk_itens_marcas');
			$table->dropForeign('fk_itens_fornecedores');
			$table->dropForeign('fk_itens_ncms');
			$table->dropForeign('fk_itens_tratamentos');
			$table->dropForeign('fk_itens_grupos2');
			$table->dropForeign('fk_itens_segmentos');
			$table->dropForeign('fk_itens_cnaes');
			$table->dropForeign('fk_itens_cores');
			$table->dropForeign('fk_itens_usuarios');
			$table->dropForeign('fk_itens_planos_contas_custos');
			$table->dropForeign('fk_itens_itens');
			$table->dropForeign('fk_itens_lista_servicos');
		});
	}

}
