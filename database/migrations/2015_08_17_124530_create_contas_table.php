<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 40)->nullable();
			$table->float('saldo', 10, 0)->nullable();
			$table->float('saldo_moeda1', 10, 0)->nullable();
			$table->float('saldo_moeda2', 10, 0)->nullable();
			$table->integer('filial_id')->index('fk_contas_filiais1_idx');
			$table->string('conta_contabil', 20)->nullable();
			$table->boolean('fluxo')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('conta_bancaria_id')->index('fk_contas_contas_bancarias1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contas');
	}

}
