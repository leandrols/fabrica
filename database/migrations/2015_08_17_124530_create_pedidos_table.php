<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cliente_id')->index('fk_pedidos_clientes1_idx');
			$table->integer('vendedor_colaborador_id')->index('fk_pedidos_colaboradores1_idx');
			$table->integer('entregador_colaborador_id')->nullable()->index('fk_pedidos_colaboradores2_idx');
			$table->integer('separador_colaborador_id')->nullable()->index('fk_pedidos_colaboradores3_idx');
			$table->integer('conferente_colaborador_id')->nullable()->index('fk_pedidos_colaboradores4_idx');
			$table->integer('atendente_colaborador_id')->index('fk_pedidos_colaboradores5_idx');
			$table->integer('vendedor_externo_colaborador_id')->nullable()->index('fk_pedidos_colaboradores6_idx');
			$table->integer('tipo_realizacao_id')->index('fk_pedidos_tipo_realizacoes1_idx');
			$table->integer('forma_id')->index('fk_pedidos_formas1_idx');
			$table->integer('prazo_id')->nullable()->index('fk_pedidos_prazos1_idx');
			$table->integer('filial_id')->index('fk_pedidos_filiais1_idx');
			$table->integer('indicante_cliente_id')->nullable()->index('fk_pedidos_clientes2_idx');
			$table->integer('transportadora_id')->nullable()->index('fk_pedidos_transportadoras1_idx');
			$table->integer('pai_pedido_id')->nullable()->index('fk_pedidos_pedidos1_idx');
			$table->integer('ordem_producao_id')->nullable()->index('fk_pedidos_ordem_producoes1_idx');
			$table->integer('ordem_servico_id')->nullable()->index('fk_pedidos_ordem_servicos1_idx');
			$table->integer('lista_entrega_id')->nullable()->index('fk_pedidos_lista_entregas1_idx');
			$table->integer('cliente_rede_id')->nullable()->index('fk_pedidos_clientes_redes1_idx');
			$table->integer('periodo_entrega_id')->nullable()->index('fk_pedidos_periodos_entregas1_idx');
			$table->integer('endereco_id')->nullable()->index('fk_pedidos_enderecos1_idx');
			$table->integer('cfop_id')->index('fk_pedidos_cfops1_idx');
			$table->integer('projetos_id')->nullable()->index('fk_pedidos_projetos1_idx');
			$table->float('total_producao', 10, 0)->unsigned()->default(0);
			$table->float('total_custo', 10, 0)->unsigned()->default(0);
			$table->float('total_bruto', 10, 0)->unsigned()->default(0);
			$table->float('total_liquido', 10, 0)->unsigned()->default(0);
			$table->float('total_comissao', 10, 0)->unsigned()->default(0);
			$table->float('total_servico', 10, 0)->unsigned()->default(0);
			$table->float('total_vic', 10, 0)->unsigned()->default(0);
			$table->float('total_base_icms', 10, 0)->unsigned()->default(0);
			$table->float('total_icms', 10, 0)->unsigned()->default(0);
			$table->float('total_ipi', 10, 0)->unsigned()->default(0);
			$table->float('total_subs', 10, 0)->unsigned()->default(0);
			$table->float('total_base_subs', 10, 0)->unsigned()->default(0);
			$table->float('total_cofins', 10, 0)->unsigned()->default(0);
			$table->float('total_iss', 10, 0)->unsigned()->default(0);
			$table->float('valor_desconto', 10, 0)->unsigned()->default(0);
			$table->float('valor_frete', 10, 0)->unsigned()->default(0);
			$table->float('valor_seguro', 10, 0)->unsigned()->default(0);
			$table->float('valor_outras', 10, 0)->unsigned()->default(0);
			$table->integer('numero_parcelas')->unsigned()->default(1);
			$table->text('obs', 65535)->nullable();
			$table->boolean('entregue')->default(0);
			$table->enum('faturado', array('Aguardando','Não','Sim','Expedição','Liberação'))->default('Não');
			$table->boolean('pago')->default(0);
			$table->boolean('separado')->default(0);
			$table->float('peso_bruto', 10, 0)->unsigned()->default(0);
			$table->float('peso_liquido', 10, 0)->unsigned()->default(0);
			$table->float('margem_desconto', 10, 0)->unsigned()->default(0);
			$table->date('data_faturamento')->nullable();
			$table->boolean('sem_fatura')->default(0);
			$table->string('razao', 50);
			$table->string('telefone', 11)->nullable();
			$table->string('email', 60)->nullable();
			$table->float('indice', 10, 0)->unsigned()->default(0);
			$table->string('motivo_acressimo', 50)->nullable();
			$table->float('valor_acressimo', 10, 0)->unsigned()->default(0);
			$table->string('controle', 22)->nullable();
			$table->float('itens_quantidade', 10, 0)->unsigned()->default(0);
			$table->text('obs_internas', 65535)->nullable();
			$table->enum('tipo_frete', array('CIF','FOB'))->nullable();
			$table->boolean('retirar_loja')->default(1);
			$table->string('entrega_comprador', 50)->nullable();
			$table->string('entrega_telefone', 11)->nullable();
			$table->boolean('entrega_oculto')->default(0);
			$table->string('entrega_obra', 50)->nullable();
			$table->dateTime('data_saida');
			$table->dateTime('data_entrega')->nullable();
			$table->boolean('mobile')->default(0);
			$table->boolean('mobile_sync')->default(0);
			$table->string('natureza', 60);
			$table->boolean('pago_comissao_vendedor1')->default(0);
			$table->boolean('pago_comissao_vendedor2')->default(0);
			$table->boolean('pago_comissao_atendente')->default(0);
			$table->boolean('pago_comissao_cobrador')->default(0);
			$table->boolean('pago_comissao_indicante')->default(0);
			$table->boolean('pago_comissao_entregador')->default(0);
			$table->boolean('pago_comissao_separador')->default(0);
			$table->float('cotacao_moeda1', 10, 0)->unsigned()->default(0);
			$table->float('cotacao_moeda2', 10, 0)->unsigned()->default(0);
			$table->boolean('confirmacao_1')->default(0);
			$table->boolean('confirmacao_2')->default(0);
			$table->boolean('confirmacao_3')->default(0);
			$table->string('voucher', 50)->nullable();
			$table->boolean('encomenda')->nullable()->default(0);
			$table->boolean('enviar_sms')->default(1);
			$table->boolean('enviar_email')->default(1);
			$table->boolean('locacao')->default(0);
			$table->string('ordem_compra', 15)->nullable();
			$table->boolean('cliente_recebeu')->nullable()->default(0);
			$table->text('cliente_recebeu_obs', 65535)->nullable();
			$table->boolean('excluido_multa')->nullable()->default(0);
			$table->boolean('convertido')->default(0);
			$table->boolean('parcial')->default(0);
			$table->integer('id_mobile')->nullable();
			$table->integer('id_web')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos');
	}

}
