<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVolumesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('volumes', function(Blueprint $table)
		{
			$table->foreign('pedidos_id', 'fk_volumes_pedidos')->references('id')->on('pedidos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('uf_id', 'fk_volumes_ufs')->references('id')->on('ufs')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('volumes', function(Blueprint $table)
		{
			$table->dropForeign('fk_volumes_pedidos');
			$table->dropForeign('fk_volumes_ufs');
		});
	}

}
