<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensRestricoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_restricoes', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_itens_restricoes_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('atividade_id', 'fk_itens_restricoes_atividades')->references('id')->on('atividades')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_restricoes', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_restricoes_itens');
			$table->dropForeign('fk_itens_restricoes_atividades');
		});
	}

}
