<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ferias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('data_inicio')->nullable();
			$table->date('data_fim')->nullable();
			$table->integer('colaboradores_id')->index('fk_ferias_colaboradores1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ferias');
	}

}
