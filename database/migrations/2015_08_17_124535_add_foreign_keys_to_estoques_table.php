<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEstoquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('estoques', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_estoques_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_estoques_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('fornecedor_id', 'fk_estoques_fornecedores')->references('id')->on('fornecedores')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('estoques', function(Blueprint $table)
		{
			$table->dropForeign('fk_estoques_itens');
			$table->dropForeign('fk_estoques_filiais');
			$table->dropForeign('fk_estoques_fornecedores');
		});
	}

}
