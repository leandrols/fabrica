<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clientes', function(Blueprint $table)
		{
			$table->foreign('endereco_id', 'fk_clientes_enderecos1')->references('id')->on('enderecos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cobranca_endereco_id', 'fk_clientes_enderecos2')->references('id')->on('enderecos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('forma_id', 'fk_clientes_formas')->references('id')->on('formas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vendedor_colaborador_id', 'fk_clientes_colaboradores1')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vendedor_externo_colaborador_id', 'fk_clientes_colaboradores2')->references('id')->on('colaboradores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('setor_id', 'fk_clientes_setores')->references('id')->on('setores')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('tabela_id', 'fk_clientes_tabelas')->references('id')->on('tabelas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('transportadora_id', 'fk_clientes_transportadoras')->references('id')->on('transportadoras')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('atividade_id', 'fk_clientes_atividades')->references('id')->on('atividades')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('prazo_id', 'fk_clientes_prazos')->references('id')->on('prazos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('clientes_rede_id', 'fk_clientes_clientes_redes')->references('id')->on('clientes_redes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('clientes_grupo_id', 'fk_clientes_clientes_grupos')->references('id')->on('clientes_grupos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('indicante_cliente_id', 'fk_clientes_clientes')->references('id')->on('clientes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('classificacao_id', 'fk_clientes_classificacao')->references('id')->on('classificacoes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('porte_id', 'fk_clientes_portes')->references('id')->on('portes')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clientes', function(Blueprint $table)
		{
			$table->dropForeign('fk_clientes_enderecos1');
			$table->dropForeign('fk_clientes_enderecos2');
			$table->dropForeign('fk_clientes_formas');
			$table->dropForeign('fk_clientes_colaboradores1');
			$table->dropForeign('fk_clientes_colaboradores2');
			$table->dropForeign('fk_clientes_setores');
			$table->dropForeign('fk_clientes_tabelas');
			$table->dropForeign('fk_clientes_transportadoras');
			$table->dropForeign('fk_clientes_atividades');
			$table->dropForeign('fk_clientes_prazos');
			$table->dropForeign('fk_clientes_clientes_redes');
			$table->dropForeign('fk_clientes_clientes_grupos');
			$table->dropForeign('fk_clientes_clientes');
			$table->dropForeign('fk_clientes_classificacao');
			$table->dropForeign('fk_clientes_portes');
		});
	}

}
