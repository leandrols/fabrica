<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensProcessosValoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_processos_valores', function(Blueprint $table)
		{
			$table->integer('item_processo_id')->index('fk_item_processo_campo_valores_item_processos1_idx');
			$table->integer('processo_campo_id')->index('fk_item_processo_campo_valores_processo_campos1_idx');
			$table->string('valor');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_processos_valores');
	}

}
