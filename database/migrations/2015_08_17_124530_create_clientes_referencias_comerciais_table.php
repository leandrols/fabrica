<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesReferenciasComerciaisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes_referencias_comerciais', function(Blueprint $table)
		{
			$table->integer('cliente_id')->index('fk_clientes_referencias_comerciais_clientes1_idx');
			$table->integer('referencia_comercial_id')->index('fk_clientes_referencias_comerciais_referencias_comerciais1_idx');
			$table->primary(['cliente_id','referencia_comercial_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes_referencias_comerciais');
	}

}
