<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnderecosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enderecos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nome', 40)->nullable();
			$table->string('cep', 8);
			$table->string('logradouro', 70);
			$table->integer('numero')->nullable();
			$table->string('complemento', 30)->nullable();
			$table->string('bairro', 50);
			$table->integer('cidade_id')->index('fk_enderecos_cidades1_idx');
			$table->enum('tipo', array('Entrega','Cobranca','Pessoal'))->default('Pessoal');
			$table->float('latitude', 10, 0)->nullable();
			$table->float('longitude', 10, 0)->nullable();
			$table->string('referencia', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('enderecos');
	}

}
