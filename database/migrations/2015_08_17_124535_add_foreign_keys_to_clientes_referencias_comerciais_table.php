<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientesReferenciasComerciaisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clientes_referencias_comerciais', function(Blueprint $table)
		{
			$table->foreign('cliente_id', 'fk_clientes_referencias_comerciais_clientes')->references('id')->on('clientes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('referencia_comercial_id', 'fk_clientes_referencias_comerciais_referencias_comerciais')->references('id')->on('referencias_comerciais')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clientes_referencias_comerciais', function(Blueprint $table)
		{
			$table->dropForeign('fk_clientes_referencias_comerciais_clientes');
			$table->dropForeign('fk_clientes_referencias_comerciais_referencias_comerciais');
		});
	}

}
