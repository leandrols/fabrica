<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrazosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prazos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('forma_id')->index('fk_prazo_formas1_idx');
			$table->string('nome', 55);
			$table->float('valor_minimo', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prazos');
	}

}
