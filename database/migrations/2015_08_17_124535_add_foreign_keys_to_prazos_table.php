<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPrazosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('prazos', function(Blueprint $table)
		{
			$table->foreign('forma_id', 'fk_prazo_formas')->references('id')->on('formas')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('prazos', function(Blueprint $table)
		{
			$table->dropForeign('fk_prazo_formas');
		});
	}

}
