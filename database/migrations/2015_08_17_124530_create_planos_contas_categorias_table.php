<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanosContasCategoriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planos_contas_categorias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plano_conta_id')->index('fk_plano_contas_categoria_plano_contas1_idx');
			$table->string('nome', 30);
			$table->string('classificacao', 4)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planos_contas_categorias');
	}

}
