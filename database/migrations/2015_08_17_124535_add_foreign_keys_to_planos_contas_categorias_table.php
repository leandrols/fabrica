<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanosContasCategoriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('planos_contas_categorias', function(Blueprint $table)
		{
			$table->foreign('plano_conta_id', 'fk_planos_contas_categorias_planos_contas')->references('id')->on('planos_contas')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('planos_contas_categorias', function(Blueprint $table)
		{
			$table->dropForeign('fk_planos_contas_categorias_planos_contas');
		});
	}

}
