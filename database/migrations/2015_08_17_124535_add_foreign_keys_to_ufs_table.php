<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUfsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ufs', function(Blueprint $table)
		{
			$table->foreign('pais_id', 'fk_ufs_paises')->references('id')->on('paises')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ufs', function(Blueprint $table)
		{
			$table->dropForeign('fk_ufs_paises');
		});
	}

}
