<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToListasServicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('listas_servicos', function(Blueprint $table)
		{
			$table->foreign('cidades_ibge_verificador', 'fk_listas_servicos_cidades')->references('ibge_verificador')->on('cidades')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('listas_servicos', function(Blueprint $table)
		{
			$table->dropForeign('fk_listas_servicos_cidades');
		});
	}

}
