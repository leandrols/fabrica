<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItensHistoricosEstoquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('itens_historicos_estoques', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_itens_historicos_itens')->references('id')->on('itens')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('usuario_id', 'fk_itens_historicos_usuarios')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('filial_id', 'fk_itens_historicos_filiais')->references('id')->on('filiais')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('itens_historicos_estoques', function(Blueprint $table)
		{
			$table->dropForeign('fk_itens_historicos_itens');
			$table->dropForeign('fk_itens_historicos_usuarios');
			$table->dropForeign('fk_itens_historicos_filiais');
		});
	}

}
