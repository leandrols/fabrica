<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItensProcessosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itens_processos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_item_processos_itens1_idx');
			$table->integer('processo_id')->index('fk_item_processos_processos1_idx');
			$table->string('obs')->nullable();
			$table->integer('vinculado_item_id')->nullable()->index('fk_item_processos_itens2_idx');
			$table->integer('ordem');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itens_processos');
	}

}
