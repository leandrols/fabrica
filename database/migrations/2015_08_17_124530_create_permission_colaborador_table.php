<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissionColaboradorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission_colaborador', function(Blueprint $table)
		{
			$table->integer('colaboradores_id')->index('fk_colaboradores_has_permissions_colaboradores1_idx');
			$table->integer('permissions_id')->unsigned()->index('fk_colaboradores_has_permissions_permissions1_idx');
			$table->primary(['colaboradores_id','permissions_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission_colaborador');
	}

}
