<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProcessosCamposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('processos_campos', function(Blueprint $table)
		{
			$table->foreign('processo_id', 'fk_processos_campos_processos')->references('id')->on('processos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('processos_campos', function(Blueprint $table)
		{
			$table->dropForeign('fk_processos_campos_processos');
		});
	}

}
