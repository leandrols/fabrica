<!doctype html>
<html lang="pt_BR" class="no-js" ng-app="lstech"  id="extr-page">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title ng-bind="$root.title + ' - LS Tech'">LS Tech</title>

    <link rel="shortcut icon" href="assets/images/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/images/favicon/favicon.ico" type="image/x-icon">

    <!-- Dependencies -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    <link rel="stylesheet" href="{{ elixir('assets/css/dependencies.css') }}">
    <link rel="stylesheet" href="{{ elixir('assets/css/tema.css') }}">

    <!-- App -->
    <link rel="stylesheet" href="{{ elixir('assets/css/app.css') }}">

</head>
<body>

    <div ui-view></div>

    <!-- Application Dependencies -->
    <script src="{{ elixir('assets/js/dependencies.js') }}"></script>

    <!-- Application Scripts -->
    <script src="{{ elixir('assets/js/app.js') }}"></script>
</body>
</html>