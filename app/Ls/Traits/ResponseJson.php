<?php

namespace Ls\Traits;

trait ResponseJson
{
    public function responseJson($data, $status = true)
    {
        if (is_bool($status)) {
            $status = $status ? 200 : 422;
        }
        if ($status < 400 || $status > 499) {
            return response()->json($data, $status);
        }

        if (!is_array($data) && !is_object($data)) {
            $data = array('message' => $data);
        }

        return response()->json(array('error' => $data, 'status' => $status), $status);
    }
}
