<?php

namespace Ls\Sms\Services;

class SmsGlobalService extends SmsAbstractService
{
    /**
     * Url para enviar mensagens.
     *
     * @var string
     */
    protected $url = 'http://www.smsglobal.com/http-api.php';
    /**
     * Envia SMS.
     *
     * @param string|int $phone
     * @param string     $text
     *
     * @return [type] [description]
     */
    public function send($to, $text, $from = false)
    {
        if ($from) {
            $this->from = $from;
        }

        $fields = array(
                'action'   => 'sendsms',
                'user'     => urlencode($this->user),
                'password' => urlencode($this->pass),
                'to'       => urlencode($to),
                'maxsplit' => $this->maxsplit,
                'text'     => urlencode($text),
            );

        if ($this->from != '') {
            $fields[] = array('from' => urlencode($this->from));
        }

        if ($this->setScheduled) {
            $fields[] = array('scheduled' => urlencode($this->scheduled));
        }

        //url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key.'='.$value.'&';
        }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        $pattern = '/SMSGLOBAL DELAY MSGID:/';
        $pattern = '/SMSGlobalMsgID:([0-9]+)/';
//        $pattern = '/SMSGlobalMsgID: ?([0-9]+)/';
        $matches = array();

        # Executa nossa expressão
        if (!preg_match($pattern, $result, $matches)) {
            return false;
        }

        return $matches[1];
    }
}
