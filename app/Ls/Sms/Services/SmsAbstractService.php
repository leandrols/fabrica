<?php

namespace Ls\Sms\Services;

/**
 * Envio de SMS.
 */
abstract class SmsAbstractService
{
    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var string
     */
    private $from = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var int
     */
    protected $maxsplit = '160';

     /**
      * @var timestamps
      */
     protected $scheduled = false;

    /**
     * Classe construtora.
     *
     * @param string $user
     * @param string $pass
     * @param string $from
     */
    public function __construct($user, $pass, $from = '')
    {
        $this->user = $user;
        $this->pass = $pass;
        $this->from = $from;
    }

    /**
     * Metodo para envio de SMS.
     *
     * @param [type] $text
     *
     * @return [type]
     */
    abstract public function send($text);

    /**
     * set o nome do usuário.
     *
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * seta a senha.
     *
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * seta a url de envio.
     *
     * @param [type] $url
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * seta os dados de origem.
     *
     * @param string|int $from
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Tamanho máximo, dividir mensagens após este número.
     *
     * @param int $max
     */
    public function setMaxsplit($max)
    {
        $this->maxsplit = $max;

        return $this;
    }

    /**
     * data e hora para agendar mensagem.
     *
     * @param timestamps $datetime
     */
    public function setScheduled($datetime)
    {
        $this->scheduled = $datetime;

        return $this;
    }
}
