<?php

namespace Ls\Log\Repositories;

use Ls\Log\Entities\Log;
use Illuminate\Container\Container as App;
use Exception;

/**
 * Repositório de Log.
 */
class LogRepository
{
    public function create(array $data)
    {
        $app = new App();
        $log = $app->make('Ls\Log\Entities\Log');

        $data['content']    = json_encode($data['content']);
        $data['created_at'] = date('Y-m-d H:i:s');
        $log->fill($data);
        try {
            $log->save();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * pesquisar Log.
     *
     * @param array $data
     * @param array $columns
     *
     * @return \Ls\Log\Entities\Log
     */
    public function search(array $data, $columns = ['*'])
    {
        $app = new App();
        $log = $app->make('Ls\Log\Entities\Log');
        if (count($data) === 0) {
            return false;
        }
        if (isset($data['id'])) {
            return $log->where('id', $data['id'])->get();
        }
        if (isset($data['ids'])) {
            $log = $log->whereIn('id', explode(',', $data['ids']));
        }
        if (isset($data['content'])) {
            $log = $log->where('content', 'ilike', '%'.$data['content'].'%');
        }
        if (isset($data['identifier'])) {
            $log = $log->where('identifier', $data['identifier']);
        }
        if (isset($data['type'])) {
            $log = $log->where('type', $data['type']);
        }
        if (isset($data['user_id'])) {
            $log = $log->where('user_id', $data['user_id']);
        }
        if (isset($data['user_ids'])) {
            $log = $log->whereIn('user_ids', explode(',', $data['user_ids']));
        }
        if (isset($data['created_at'])) {
            $log = $log->where('created_at', 'ilike', $data['created_at'].'%');
        }
        if (isset($data['start']) && isset($data['end'])) {
            $log = $log->whereBetween('created_at', [$data['start'], $data['end']]);
        } elseif (isset($data['start'])) {
            $log = $log->where('created_at', '>=', $data['start']);
        } elseif (isset($data['end'])) {
            $log = $log->where('created_at', '<=', $data['end']);
        }
        try {
            $result = $log->get($columns);
            if (count($result) < 1) {
                return false;
            }

            return $result;
        } catch (Exception $e) {
            return false;
        }
    }
}
