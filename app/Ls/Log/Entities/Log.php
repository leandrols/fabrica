<?php

namespace Ls\Log\Entities;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $table       = 'logs';
    protected $fillable = ['identifier','content','user_id','created_at','type'];
    public $timestamps  = false;
}
