<?php

namespace Ls\Log\Services;

use Ls\Log\Repositories\LogRepository;

/**
 * Class log.
 */
class LogService
{
    /**
     * cria log.
     *
     * @param array $data
     */
    public function create(array $data)
    {
        //['identifier','content','user_id','created_at','type'];

        $data['user_id'] = $this->getUser();
        $repo            = new LogRepository();
        $repo->create($data);
    }

    /**
     * retorna o usuário atual.
     *
     * @return [type] [description]
     */
    private function getUser()
    {
        return 1;
    }
}
