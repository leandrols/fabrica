<?php

namespace Ls\Contracts;

interface RepositoryInterface
{
    public function all($columns = ['*']);

    public function paginate($perPage = 10, $columns = ['*']);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id, $multi = false);

    public function find($id, $columns = ['*']);

    public function findBy($field, $value, $columns = ['*']);

    public function findById($value, $columns = ['*']);

    public function findByIds($value, $columns = ['*']);
}
