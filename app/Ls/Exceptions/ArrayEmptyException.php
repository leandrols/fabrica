<?php

namespace Ls\Exceptions;

class ArrayEmptyException extends \Exception
{
    protected $message = 'Array vazia';
    protected $code    = 403;
}
