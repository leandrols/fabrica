<?php

namespace Ls\Exceptions;

class MultiDeleteException extends \Exception
{
    protected $message = 'O campo multiDelete deverá ter o valor verdadeiro ou falso.';
    protected $code    = 403;
}
