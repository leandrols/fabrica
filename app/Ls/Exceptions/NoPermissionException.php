<?php

namespace Ls\Exceptions;

class NoPermissionException extends \Exception
{
    protected $message = 'Sem permissão de acesso';
    protected $code    = 403;
}
