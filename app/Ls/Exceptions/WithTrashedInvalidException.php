<?php

namespace Ls\Exceptions;

class WithTrashedInvalidException extends \Exception
{
    protected $message = 'withTrashed inválido';
    protected $code    = 403;
}
