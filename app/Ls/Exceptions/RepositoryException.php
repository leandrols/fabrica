<?php

namespace Ls\Exceptions;

/**
 * RepositoryException.
 */
class RepositoryException
{
    protected $code = 404;
}
