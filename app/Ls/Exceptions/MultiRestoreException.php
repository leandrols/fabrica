<?php

namespace Ls\Exceptions;

class MultiRestoreException extends \Exception
{
    protected $message = 'O campo multiRestore deverá ter o valor verdadeiro ou falso.';
    protected $code    = 403;
}
