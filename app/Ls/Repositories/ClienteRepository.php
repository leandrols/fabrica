<?php

namespace Ls\Repositories;

/**
 * ClienteRepository.
 */
class ClienteRepository extends Repository
{
    public function model()
    {
        return 'App\Cliente';
    }
}
