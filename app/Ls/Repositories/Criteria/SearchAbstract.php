<?php

namespace Ls\Repositories\Criteria;

class SearchAbstract
//abstract class SearchAbstract
{
    private $query;

    public function setQuery(&$query)
    {
        $this->query = &$query;
    }

    public function testador($value)
    {
        $this->query .= '-'.$value;

        return;
        $this->query .= '-'.$value;
    }

    public function id($value)
    {
        if (is_array($value)) {
            $this->in('id', $value);

            return;
        }
        $this->query = $this->query->where('nome', 'ilike', $value.'%');
    }

    public function nome($value)
    {
        if (is_array($value)) {
            $this->in('nome', $value);

            return;
        }
        $this->query = $this->query->where('nome', 'ilike', $value.'%');
    }

    public function data($value)
    {
        if (!is_array($value) || count($value) < 2) {
            $this->query = $this->query->where('data', $value);

            return;
        }
        if (count($value) > 2) {
            $this->in('data', $value);

            return;
        }
        if (is_null($value[0]) || $value[0]  == 'null') {
            $this->query = $this->query->where('data', '>=', $value[0]);

            return;
        }

        if (is_null($value[1]) || $value[1]  == 'null') {
            $this->query = $this->query->where('data', '<=', $value[0]);

            return;
        }
        $this->query = $this->between('data', $value[0], $value[1]);
    }

    public function between($field, $start, $end)
    {
        $this->query = $this->query->whereBetween($field, $start, $end);
    }

    public function in($field, $value)
    {
        if (!is_array($value)) {
            $value = array($value);
        }
        $this->query = $this->query->whereIn($field, $value);
    }
}

$query = 'inicio';
$a     = new SearchAbstract();
$a->setQuery($query);
$a->testador('dois');
$a->testador('três');
echo $query."\n";

$table = [
    'table'  => 'clientes',
    'fields' => [
        'id' => [
            'type'     => 'serial',
            'required' => true,
        ],
        'razao' => [
            'type' => 'character',
            'size' => 60,
//            'new'  => '15/08/2016',
        ],
        'fantasia' => [
            'type' => 'character',
            'size' => 60,
        ],
        'cnpj' => [
            'type' => 'character',
            'size' => 14,
        ],
        'ie' => [
            'type' => 'character',
            'size' => 20,
        ],
        'endereco_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'cobranca_endereco_id' => [
            'type'         => 'integer',
            'size'         => 10,
            'required'     => true,
            'relationship' => [
                'table'  => 'enderecos',
                'id'     => 'id',
                'search' => 'cobrancaEndereco',
                ],
        ],
        'email' => [
            'type' => 'character',
            'size' => 60,
        ],
        'telefone' => [
            'type'     => 'character',
            'size'     => 11,
            'required' => true,
        ],
        'ramal' => [
            'type' => 'character',
            'size' => 5,
        ],
        'telefone2' => [
            'type' => 'character',
            'size' => 11,
        ],
        'fax' => [
            'type' => 'character',
            'size' => 11,
        ],
        'forma_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'vendedor_colaborador_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'vendedor_externo_colaborador_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'status' => [
            'type'   => 'character',
            'values' => ['Ativo','Inativo','Bloqueado','A vista'],
        ],
        'referencia_agencia' => [
            'type' => 'character',
            'size' => 20,
        ],
        'referencia_banco' => [
            'type' => 'character',
            'size' => 20,
        ],
        'referencia_conta' => [
            'type' => 'character',
            'size' => 20,
        ],
        'referencia_data_abertura' => [
            'type' => 'date',
        ],
        'referencia_gerente' => [
            'type' => 'character',
            'size' => 20,
        ],
        'referencia_telefone_gerente' => [
            'type' => 'character',
            'size' => 11,
        ],
        'referencia_obs' => [
            'type' => 'text',
        ],
        'nascimento' => [
            'type' => 'date',
        ],
        'setor_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'tabela_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'iss' => [
            'type' => 'character',
            'size' => 14,
        ],
        'usa_cartao' => [
            'type'     => 'boolean',
            'required' => true,
            'default'  => false,
        ],
        'bandeira_cartao' => [
            'type' => 'character',
            'size' => 30,
        ],
        'usa_cheque' => [
            'type'     => 'boolean',
            'required' => true,
            'default'  => false,
        ],
        'titulo_eleitor' => [
            'type' => 'character',
            'size' => 30,
        ],
        'habilitacao' => [
            'type' => 'character',
            'size' => 30,
        ],
        'rg' => [
            'type' => 'character',
            'size' => 30,
        ],
        'obs' => [
            'type' => 'text',
        ],
        'desconto' => [
            'type'     => 'numeric',
            'required' => true,
            'default'  => 0,
        ],
        'limite' => [
            'type'     => 'numeric',
            'required' => true,
            'default'  => 0,
        ],
        'transportadora_id' => [
            'type' => 'integer',
        ],
        'atividade_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'pai' => [
            'type' => 'character',
            'size' => 60,
        ],
        'mae' => [
            'type' => 'character',
            'size' => 60,
        ],
        'site' => [
            'type' => 'character',
            'size' => 45,
        ],
        'prazo_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'consulta_data' => [
            'type' => 'date',
        ],
        'consulta_obs' => [
            'type' => 'character',
            'size' => 80,
        ],
        'motivo' => [
            'type' => 'character',
            'size' => 80,
        ],
        'preco_custo' => [
            'type'     => 'boolean',
            'required' => true,
            'default'  => false,
        ],
        'pais_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'exterior_uf' => [
            'type' => 'character',
            'size' => 30,
        ],
        'exterior_telefone1' => [
            'type' => 'character',
            'size' => 30,
        ],
        'exterior_telefone2' => [
            'type' => 'character',
            'size' => 30,
        ],
        'suframa' => [
            'type' => 'character',
            'size' => 9,
        ],
        'margem_comissao' => [
            'type' => 'numeric',
        ],
        'iest' => [
            'type' => 'character',
            'size' => 20,
        ],
        'clientes_rede_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'clientes_grupo_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'senha' => [
            'type' => 'character',
            'size' => 50,
        ],
        'consignatario' => [
            'type' => 'text',
        ],
        'notify' => [
            'type' => 'text',
        ],
        'regime' => [
            'type'   => 'character',
            'values' => ['Simples','Normal'],
        ],
        'sexo' => [
            'type'   => 'character',
            'values' => ['Masculino','Feminino'],
        ],
        'notificacao_sms' => [
            'type'     => 'boolean',
            'required' => true,
            'default'  => false,
        ],
        'notificacao_email' => [
            'type'     => 'boolean',
            'required' => true,
            'default'  => false,
        ],
        'celular' => [
            'type' => 'character',
            'size' => 11,
        ],
        'dia_fatura1' => [
            'type' => 'smallint',
        ],
        'dia_fatura2' => [
            'type' => 'smallint',
        ],
        'dia_fatura3' => [
            'type' => 'smallint',
        ],
        'dia_fatura_semanal' => [
            'type'   => 'character',
            'values' => ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        ],
        'indicante_cliente_id' => [
            'type' => 'integer',
        ],
        'cobrar_taxa_boleto' => [
            'type'     => 'boolean',
            'required' => true,
            'default'  => false,
        ],
        'indice' => [
            'type'     => 'numeric',
            'required' => true,
        ],
        'classificacao_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'data_bloqueio' => [
            'type' => 'date',
        ],
        'desconto_colaborador' => [
            'type'     => 'numeric',
            'required' => true,
            'default'  => 0,
        ],
        'porte_id' => [
            'type'     => 'integer',
            'required' => true,
        ],
        'cartao_fidelidade' => [
            'type' => 'character',
            'size' => 32,
        ],
        'responsavel' => [
            'type' => 'character',
            'size' => 100,
        ],
        'dia_limite_fatura' => [
            'type' => 'smallint',
        ],
        'limite_compra' => [
            'type' => 'numeric',
        ],
        'pode_ser_indicante' => [
            'type' => 'boolean',
        ],
    ],
];
//print_r($table);
//echo json_encode($table);

