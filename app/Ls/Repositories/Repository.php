<?php

namespace Ls\Repositories;

use Ls\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Ls\Exceptions\RepositoryException;
use Exception;
use Ls\Exceptions\MultiDeleteException;
use Ls\Exceptions\MultiRestoreException;
use Ls\Exceptions\ArrayEmptyException;
use Ls\Exceptions\WithTrashedInvalidException;

/**
 * Abstract Repository.
 *
 * @author Neri Junior <neri@nerijunior.com>
 * @author Leandro Henrique <henrique@henriquereis.com>
 */
abstract class Repository implements RepositoryInterface
{
    private $app;

    protected $model;

    protected $requestWithTrashed = false;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();

        //Checa se serão buscados também os registros apagados
        $input = \Request::all();
        if (isset($input['withTrashed'])) {
            if ($input['withTrashed'] == 'false' || $input['withTrashed'] == '0') {
                $input['withTrashed'] = false;
            } elseif ($input['withTrashed'] == 'true' || $input['withTrashed'] == '1') {
                $input['withTrashed'] = true;
            }
            $this->requestWithTrashed = $input['withTrashed'];
        }
        $trashed = $this->requestWithTrashed;
        if ($trashed !== true && $trashed !== false && $trashed !== 'only') {
            throw new WithTrashedInvalidException();
        }
    }

    abstract public function model();

    /**
     * devolve tudo da entidade.
     *
     * @param array $columns
     *
     * @return bool|\Illuminate\Database\Eloquent\Collection
     */
    public function all($columns = ['*'])
    {
        $model = $this->modelWithTrashed();

        return $model->get($columns);
    }

    /**
     * devolve a entidade paginada.
     *
     * @param int   $perPage [description]
     * @param array $columns [description]
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function paginate($perPage = 10, $columns = ['*'])
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * cria um item da entidade.
     *
     * @param array $data [description]
     *
     * @return bool|\Illuminate\Database\Eloquent\Collection
     */
    public function create(array $data)
    {
        if (count($data) < 1) {
            throw new ArrayEmptyException();
        }
        $model = new $this->model();
        $model->fill($data);
        try {
            $model->save();

            return $this->findById([$model->id]);
        } catch (Exception $e) {
            dd($e->getMessage());

            return false;
        }
    }

    /**
     * atualiza um item da entidade.
     *
     * @param array $data
     * @param int   $id
     *
     * @return bool|\Illuminate\Database\Eloquent\Collection
     */
    public function update(array $data, $id)
    {
        if (count($data) < 1) {
            throw new ArrayEmptyException();
        }
        $model = $this->findOnlyWith($id);
        if (is_null($model)) {
            return false;
        }
        $model->fill($data);
        try {
            $model->save();

            return $this->findById($id);
        } catch (Exception $e) {
            dd($e->getMessage());

            return false;
        }
    }

    /**
     * apagar um ou vários itens da entidade.
     *
     * @param int|array|string $ids
     * @param int|bool|string  $multi
     *
     * @return int
     */
    public function delete($ids, $multi = false)
    {
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }
        if (!$multi && count($ids) > 1) {
            throw new MultiDeleteException();
        }

        return $this->model->destroy($ids, $multi);
    }

    /**
     * localiza um item da entidade.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return [type] [description]
     */
    public function find($id, $columns = ['*'])
    {
        return $this->modelWithTrashed()->find($id, $columns);
    }

    /**
     * localiza um item da entidade apenas com as relações informadas.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return \Ls\Models\Model|null
     */
    public function findOnlyWith($id, $columns = ['*'], array $with = [])
    {
        $model = $this->modelWithTrashed($this->model->onlyWith($with));

        return $model->find($id, $columns);
    }

    /**
     * Verifica se está solicitando registros apagados.
     *
     * @param bool $trashed [description]
     *
     * @return \Ls\Models\Model|null
     */
    protected function modelWithTrashed($model = false)
    {
        if ($model) {
            $this->model = $model;
        }
        if (!$this->requestWithTrashed || !in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this->model))) {
            return $this->model;
        }
        if ($this->requestWithTrashed == 'only') {
            return $this->model->onlyTrashed();
        }
        if ($this->requestWithTrashed === true) {
            return $this->model->withTrashed();
        }

        return $this->model;
    }

    /**
     * localiza pelo campo.
     *
     * @param string $field   [description]
     * @param string $value   [description]
     * @param array  $columns [description]
     *
     * @return \Ls\Models\Model|null
     */
    public function findBy($field, $value, $columns = ['*'])
    {
        return $this->modelWithTrashed()->where($field, '=', $value)->get($columns);
    }

    /**
     * Localiza pelo id.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findById($id, $columns = ['*'])
    {
        return $this->modelWithTrashed()->where('id', $id)->get($columns);
    }

    /**
     * Localiza pelos ids.
     *
     * @param array|string $ids
     * @param array        $columns [description]
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByIds($ids, $columns = ['*'])
    {
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        return $this->modelWithTrashed()->whereIn('id', $ids)->get($columns);
    }

    /**
     * Localiza pelos ids.
     * e traz apenas com os relacionamentos específicos.
     *
     * @param array|string $ids
     * @param array        $columns [description]
     *
     * @return array                                    $width
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByIdsOnlyWith($ids, $columns = ['*'], array $with = [])
    {
        $model = $this->modelWithTrashed($this->model->onlyWith($with));

        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        return $model->whereIn('id', $ids)->get($columns);
    }

    /**
     * pesquisar.
     *
     * @param array       $data
     * @param array       $columns
     * @param bool|string $trashed
     *
     * @return {MODEL}|bool
     */
    public function search(array $data, $columns = ['*'])
    {
        if (count($data) === 0) {
            return false;
        }

        /*
         * opções de relacionamentos
         * @var array
         */
        $optionsRelationships = [
            'referencias',
            'endereco',
            'cobrancaEndereco',
            'forma',
            'vendedorColaborador',
            'vendedorExternoColaborador',
            'setor',
            'tabela',
            'transportadora',
            'atividade',
            'prazo',
            'pais',
            'clientesRede',
            'clientesGrupo',
            'indicanteCliente',
            'classificacao',
            'porte',
        ];
        $relationships = array();

        /*
         * relacionamentos informados necessários
         */
        if (isset($data['relationships'])) {
            $relationships = $data['relationships'];
            if (!is_array($relationships)) {
                $relationships = array($data['relationships']);
            }
        }

        /*
         * adiciona os relacionamentos
         * que estão sendo requirido buscas
         * por meio do request relacionamento[CAMPO]
         */
        foreach ($optionsRelationships as $value) {
            if (isset($data[$value]) && is_array($data[$value])) {
                $relationships[] = $value;
            }
        }

        /*
         * Busca os dados e traz
         * apenas os relacionamentos
         * que estão sendo informados
         * @var [type]
         */
        $model = $this->modelWithTrashed($this->model->onlyWith($relationships));

        /*
         * se o campo ID for informado
         * termina a busca
         * trazendo os dados referente ao id informado
         */
        if (isset($data['id'])) {
            $result = $this->findById($data['id'], $columns, $this->requestWithTrashed);
            if (count($result) < 1) {
                return false;
            }

            return $result;
        }

        return false;
    }

    /**
     * restaura.
     *
     * @param int|array|string $ids
     * @param int|bool|string  $multi
     *
     * @return int|bool
     */
    public function restore($ids, $multi, $field = 'id')
    {
        if (!in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this->model))) {
            return false;
        }

        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }
        if (!$multi && count($ids) > 1) {
            throw new MultiRestoreException();
        }

        return $this->model->withTrashed()->whereIn($field, $ids)->restore();
    }

    /**
     * makeModel.
     *
     * @return Illuminate\Database\Eloquent\Model
     *
     * @throws RepositoryException if $model is not an instance of Model
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }
}
