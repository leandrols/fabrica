<?php

namespace Ls\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Ls\Traits\ResponseJson;

abstract class Request extends FormRequest
{
    use ResponseJson;
    abstract public function rules();

    public function __construct()
    {
        /*
        $this->validator = app('validator');
        $this->validateVerdadeiroFalso($this->validator);
*/
    }
    public function validateVerdadeiroFalso($validator)
    {
        $validator->extend('verdadeiroFalso', function ($attribute, $value, $parameters) {
            if (is_bool($value) || $value == 'true' || $value == 'false' || $value == '1' || $value == '0') {
                return true;
            }

            return false;
        });
    }
    public function authorize()
    {
        return true;
    }

    public function all()
    {
        $input = parent::all();

        if (isset($input['multiDelete'])) {
            $input['multiDelete'] = $this->verdadeiroFalso($input['multiDelete']);
        }

        if (isset($input['multiRestore'])) {
            $input['multiRestore'] = $this->verdadeiroFalso($input['multiRestore']);
        }

        return $input;
    }

    /**
     * trata se um valor é verdadeiro ou falso.
     *
     * @param string|bool|null $value [description]
     *
     * @return string|bool|null
     */
    public function verdadeiroFalso($value)
    {
        if ($value == 'false' || $value == '0') {
            $value = false;
        } elseif ($value == 'true' || $value == '1') {
            $value = true;
        }

        return $value;
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever else
        //return Response::make('Permission denied foo!', 403);
        return $this->responseJson(array('message' => 'Sem permissão para esta operação ou registro não encontrado!'), 403);
    }

    // OPTIONAL OVERRIDE
    public function response(array $errors)
    {
        // If you want to customize what happens on a failed validation,
        // override this method.
        // See what it does natively here:
        // https://github.com/laravel/framework/blob/master/src/Illuminate/Foundation/Http/FormRequest.php
        return $this->responseJson($errors, 403);
    }
}
