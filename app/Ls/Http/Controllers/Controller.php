<?php

namespace Ls\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Ls\Traits\ResponseJson;

class Controller extends BaseController
{
    use ResponseJson;
}
