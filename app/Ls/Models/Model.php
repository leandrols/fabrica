<?php

namespace Ls\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
//use Jenssegers\Mongodb\Model as EloquentModel;

use Ls\Log\Services\LogService;

class Model extends EloquentModel
{
    //protected $connection = 'mongodb';

    /**
     * salvando os dados.
     *
     * @param array $options [description]
     */
    public function save(array $options = [])
    {
        // before save code
        $original           = $this->original;
        $alter              = $this->toArray();
        $type               = ($this->exists) ? 'update' : 'create';
        $data['identifier'] = $this->table;
        try {
            //$id                    = $this->odbc_primarykeys(connection_id, qualifier, owner, table);
            //$data['identifier_id'] = $original->$id;
        } catch (\Exceptions $e) {
        }
        $data['content'] = array_diff($alter, $original);
        $data['type']    = $type;
        $saved           = parent::save();
        //dd($saved);
        // after save code

        $log = new LogService();

        $log->create($data);

        return $saved;
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        // before delete code
        $data['identifier'] = $this->table;
        $data['content']    = $this->original;
        $data['type']       = 'delete';
        try {
            $id                    = $this->primaryKey;
            $data['identifier_id'] = $this->original->$id;
        } catch (\Exceptions $e) {
        }

        $deleted = parent::delete();

        // after delete code
        if ($deleted) {
            $log = new LogService();

            $log->create($data);
        }

        return $deleted;
    }

    /**
     * Traz tudo apenas com as relações indicadas na array relations.
     *
     * @param array $relations
     *
     * @return static
     */
    public static function onlyWith(array $relations = [])
    {
        $instance       = new static();
        $instance->with = $relations;

        return $instance;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (empty($value) && !is_bool($value) && !isset($value)) ? null : $value;
            }
        });

        static::updating(function ($model) {
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (empty($value) && !is_bool($value) && !isset($value)) ? null : $value;
            }
        });
    }
}
