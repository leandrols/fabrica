<?php

namespace app\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Ls\Exceptions\MultiDeleteException;
use Ls\Exceptions\MultiRestoreException;
use Ls\Exceptions\ArrayEmptyException;
use Ls\Exceptions\WithTrashedInvalidException;
use Ls\Exceptions\NoPermissionException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $e
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ArrayEmptyException) {
            return response()->json(array('error' => array('empty' => $e->getMessage()), 'status' => $e->getCode()), $e->getCode());
        }

        if ($e instanceof MultiDeleteException) {
            return response()->json(array('error' => array('multiDelete' => $e->getMessage()), 'status' => $e->getCode()), $e->getCode());
        }

        if ($e instanceof MultiRestoreException) {
            return response()->json(array('error' => array('multiRestore' => $e->getMessage()), 'status' => $e->getCode()), $e->getCode());
        }

        if ($e instanceof WithTrashedInvalidException) {
            return response()->json(array('error' => array('withTrashed' => $e->getMessage()), 'status' => $e->getCode()), $e->getCode());
        }

        if ($e instanceof NoPermissionException) {
            return response()->json(array('error' => array('Permissão' => $e->getMessage()), 'status' => $e->getCode()), $e->getCode());
        }

        return parent::render($request, $e);
    }
}
