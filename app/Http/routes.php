<?php

Route::get('/', function () {
    return view('spa');
});

Route::group(['prefix' => 'api/v1/'], function () {
    Route::post('/auth', 'AuthenticateController@authenticate');
});
