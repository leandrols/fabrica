<?php

namespace app\Http\Controllers\Site;

use Ls\Repositories\ClienteRepository as Cliente;
use App\Http\Controllers\Controller;

/**
 * ClienteController.
 */
class ClienteController extends Controller
{
    private $cliente;

    public function __construct(Cliente $cliente)
    {
        $this->cliente = $cliente;
    }

    public function getIndex()
    {
        return \Response::json($this->cliente->all());
    }
}
