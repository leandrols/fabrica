<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Modules\Usuario\Entities\Usuario;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = [
            'login' => $request->login,
        ];

        try {
            $usuario = Usuario::where($credentials)->first();
            if (is_null($usuario)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }

            $check = Hash::check($request->senha, $usuario->senha);
            if (!$check) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }

            if (!$token = JWTAuth::fromUser($usuario)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }
}
