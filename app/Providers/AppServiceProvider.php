<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        //
/*
        \DB::listen(function ($a, $b, $c) {
            print_r([$a, $b, $c]);
            echo '<hr>';
            echo '<hr>';
        });
  */
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}
