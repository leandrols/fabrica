(function(){
    angular.module('lstech', ['ui.router', 'satellizer', 'ui.bootstrap.showErrors'])
        .constant('API', {
            path : '/api/v1/'
        })
        .config(function ($stateProvider, $urlRouterProvider, $authProvider, API) {

            $urlRouterProvider.otherwise('/auth');

            var viewPath = '../views/';

            $stateProvider
                .state('auth',{
                    url : '/auth',
                    templateUrl : viewPath + 'auth/loginView.html',
                    controller : 'AuthController',
                    title : 'Login'
                });

            $authProvider.loginUrl = API.path + '/auth/login';

        })
        .run(function($rootScope){
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
                $rootScope.title = toState.title;
            });
        });
})();
(function(){
    angular.module('lstech')
        .controller('AuthController', ['$scope', function (vm) {
            // Logar
            vm.logar = function(){
                if (!vm.formLogin.$valid)
                    return;


            }

            // Recuperar Senha
            vm.recuperarSenha = function(){

            }
        }]);
})();
//# sourceMappingURL=app.js.map