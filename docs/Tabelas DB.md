# Deletados
cupom_fiscal
documento
config_report_laudo
config_nota_fiscal_serv
config_notafiscal
cliente_veic
ecf
encomenda_config
entrada_avulsa
faltas_aviso
forma_preco
forma_preco_regra
impressora
impressora_nao
integracao
item_web
item_web_sg
licenca
lista_docto
log
mobile_correio
nota_fiscal
nota_fiscal_itens
nota_servico
nota_servico_itens
oc_itens_cl
pagar_rateio_cl
pedido_encom
pesagem
pesagem_item
proposta
proposta_config
proposta_hist
proposta_itens
propostaparc
protocolo
report_conform
report_conform_item
report_laudo
report_laudo_item
senha
status
veic_marca
veic_modelo

# Gerais
acesso - usa no sistema
agenda - ~calendario~
atividade - Ramo (cliente e fornecedor)
banco - (configurancao do boleto da empresa)
bens - (moveis, imoveis)
bens_manut - manutencao do bem
cabecalho - Html (header) da proposta, orcamento, etc
chamada - Controle de chamadas telefonicas
config_saida - Configuração da tela de pedido
configuracao - Configuracao geral do sistema
consulta_ce - Consulta do cadastro do cliente pelo Cheque Express
contato - Telefones de contato
cor - Produto, Animal
editor_texto - Mudar para Documentos - salva texto pre-formatado
excluido - Log de excluidos
feriado - Lista de feriados
forma - Formas de pagamento
frete - Range de ceps
local_entrega - Locais de entregas sem vínculos
municipio
nome_banco - Nome dos bancos
pais
periodo - Periodos de entrega
uf


# Email
Email - emails recebidos
email_enviado - Enviado
email_enviado_anexo - Endereço do anexo

# Cte
cte - Ctes emitidos
cte_carta - Carta de correção
cte_comp_moto - Motoristas que compoeem o Cte
cte_comp_quant - Quantidade transportada
cte_comp_veic - Veiculos que compoeem carreta, cavalinho ( se é de terceiro, etc)
cte_comp_vp - Vale pedágio
cte_config - Configuração da tela da Cte
cte_doc - Documentos relacionados (nfe, nota manual)
cte_doc_ant - Documento anterior - (nota fiscal da nota fiscal ou cte do cte)
cte_passagem - Passagem de alfandega
cte_rodo_coleta - Documentos de Coletas
cte_valor - Valores relativos a cte (frete, pedagio)

# Boleto
boleto - Boletos emitidos
boleto_hist - historico do boleto
boleto_instr - instrucoes do banco
boleto_retorno - Retornos trados
remessa - Remessas geradas pelo sistema para o banco

# Lista de entrega
bordero - pedidos a serem entregues
bordero_itens - Itens dos pedidos
bordero_roteiro - Roteiro que vira bordero depois

# Estoque
estoque - Movimentação
inventario - Inventario ( contagem e atualização de estoque, correção )
inventario_itens - Itens

# Invoice
invoice - Lista
invoice_config - Tela
invoice_itens - Itens

# Projeto ( Ideia do condomínio )
etapa - Nomes das etapas
fase - Nome da fase
prj - Projetos
prj_comp - Composição do projeto
prj_comp_etapa - Etapa da composição
prj_comp_etapa_item - Item da Etapa da composição
prj_comp_fase -
prj_comp_fase_item -
prj_etapa -
prj_etapa_item -
prj_fase -
prj_fase_item -
prj_file -
prj_hist -
prj_item -
retirada - Retirada de material
retirada_itens - Itens

## royalty
royalty

## Representação
rp - Pedido de representação
rp_descto -
rp_itens -
rp_nf - Notas

# Fiscal
cnae - Lista..
imposto - Imposto para venda (vinc. ncm)
ncm_sh - Usar em algum webservice
nfe_cfop - CFOP
nfse_lista_serv - Lista de serviços
sit_trib - mudar para cst

# NFe
emissor - Emissor manual
emissor_config - Config da Tela
emissor_fatura - Valores relacionados aos pagamentos do emissor
emissor_itens - Itens do emissor
nf_in - Nota fiscal de entrada de mercadoria
nf_in_itens - Itens
nota_obs - Observações padrão da NF


nfe - Notas de saida
nfe_carta - Carta de correção
nfe_config - Tela
nfe_consulta - Consulta todas as notas emitidas para o CNPJ da empresa ( permite importação )
nfe_itens - Itens
nfe_parc - Faturas da nota

nfl - Nota Fiscal de Locação
nfl_itens - Itens
nfl_parc - Faturas

# NFse
config_nfse - Configuração da NFSE
nfse - Nota fiscal eletronica de serviço
nfse_item - Itens


# Financeiro
caixa - Controle de caixa
caixa_lanc - Lancamentos do caixa
cheque - Cadastro de cheques recebidos de terceiros

Relatorio de comissão
comissao - Totalizadores do relatorio - ao salvar seta como pago a comissão do pedido
comissao_itens - Itens do relatorio gerado

conta - Conta bancária
conta_movimento - Movimento da conta

duplicata - Duplicada ( nota promissória )
extrato - Trata arquivo informado pelo banco e relaciona com informações do sistema

local_pgto - Nome dos locais de pagamento

tef - Armazena as trasações
tef_cartao - Taxas

# Ponto
cartao_ponto - Batidas de ponto
cartao_ponto_rel - relatorio por colaborador dos dias
cartao_ponto_dias - depende do _rel - Historico de alterações nos dias, etc.

# Ordem de Compra
faltas - Aviso de falta do item
oc - Ordem de compra
oc_config - Tela
oc_entr - Entradas da ordem de compra
oc_entr_itens - Itens
oc_forn - Fornecedores
oc_hist - Histórico
oc_itens - Itens
oc_parc - Fatura

# Ordem de Produção
op - Ordens
op_check - Check list
op_config - Tela
op_equip - Equipamentos Utilizados
op_estoque - Itens a serem populados no estoque
op_files - Arquivos anexados
op_hist - Histórico
op_item_cota - Informações sobre o processo do item
op_item_proc - Processos para a fabricação do item
op_materia - Materia prima utilizada
op_romaneio - Lista de entrega da ordem de produção
op_romaneio_item - Itens do romaneio
op_tab_insp - Inspeção de qualidade no momento da impressão

### Orçamento - Tabela separada para não relacionar pedidos diretamente

# Ordem de Serviço
check_list - Itens a serem verificados na Ordem de serviço
os - Ordem de Serviço
os_agenda - Agendamento da ordem
os_check - Check list
os_config - Tela
os_config_check - Tela check
os_equip - Itens que serão raparados ( tipo reparados )
os_foto - Arquivos da Ordem
os_hist - Histório
os_itens - Itens
os_motivo - Motivos de ordem de serviço
os_parc - Faturas
os_status - Nomes dos status

# Contas a Pagar
pagar - Contas a pagar
pagar_config - Tela
pagar_hist
pagar_parc
pagar_rateio - Rateio do valor entre filiais
pagar_recibo - Sempre que quitada, emitir recibo

# Cliente
classifica - Classificacao do cliente ( bom, ruim, caloteiro) - Relação com cor
cliente - Clientes
cliente_agenda - Agenda
cliente_config - configuração da tela de cliente
cliente_contato - Contatos do cliente
cliente_contato_rs - Rede social do contato
cliente_credito - Historico de movimentação de crédito
cliente_destin - Destinatário do cliente
cliente_entrega - Endereço de entregas
cliente_frota - Veiculos do cliente (CTe)
cliente_grupo - Grupo economico (nissei, mercadorama, wal..)
cliente_item - Mudança de valor por item por cliente
cliente_rede - Franquia (mercadorama 1, mercadorama 2)
cliente_rs - Rede social

porte_empresa - Porte pré-cadastrado

setor - Setor onde o cliente fica

# Colaborador
colab_cm_desc - Range de Comissão do vendedor
colab_cm_meta - Comissão por meta de venda
colab_email - Configuracao do SMTP do usuario
colab_epi - Vincula item de insumo para uso do colaborador
colab_ponto - Configurações do ponto do colaborador
colab_vic - Verba de Incentivo Comercial do colaborador (fluxo)
colaborador - Cadastro do colaborador

# Transportadora
coleta - Requisição de coleta
coleta_nfs - Lista de Nfs que a transportadora esta levando
transp - Cadastro

# Plano de Contas
custo_rateio -
pl_ct - Plano de conta
pl_ct_cat - Categoria
pl_ct_cen - Centro de custo
pl_ct_cus - Custo

# portaria
portaria

profissao - Lista de profissão CBO
prazo_pgto - Prazos pré-definido



processo - Processos do item

# Profisional
profissional - Cadastro de Profissionais

quality_forn

# Contas a Receber
receber - Pedidos faturados
receber_agenda - Logs
receber_config - Tela

# Recibo
recibo - Recibos


# Filial
filial - Dados da empresa
filial_forma - Formas de pagamento para aquela filial
filial_st - Inscrição Estadual de Substituição tributária por filial
filial_vend - Vendedor padrão da filial

# Fornecedor
fornecedor - Lista
fornecedor_contato
fornecedor_credito
fornecedor_file - Lista de documentos do fornecedor
fornecedor_item -

## Item
grupo
grupo_sub
item - Lista
item_compos - Composição do item
item_config - Tela
item_estoque - Item por filial com valores
item_forn - Fornecedores do item
item_hist - Historico de ações no item
item_loc - Estoque de locação
item_perg - Briefing (HUB Evo)
item_proc - Processo do item (ordem de fabricação)
item_restr - Ramos restritos de venda
item_royalty - Direito no item e valores


item_transp - Transposição de item - (celular, vender bateria separado)
item_transp_item - Item

segmento

tratamento - Tratamento superficial ( cromar item )



# MDFe
mdfe - Manifesto de Documento Fiscal Eletrônico
mdfe_condut - Condutor
mdfe_config - Tela
mdfe_doc - Documentos envolvidor
mdfe_mun_carga - Municipio de Carga
mdfe_reboque - Reboques, caminhão, cavalinho, etc
mdfe_vale - Vale pedagio

# Motorista
motorista - Cadastro de motorista

# Telemarketing
marketing - Abertura do contato
marketing_config - Tela
marketing_info - Informações do contato
marketing_meio - Meio de contato
marketing_status - Nome dos status de telemarketing

# Chat
msg -
msg_resp


## Pedido
pedido - Pedido
pedido_agrup - Agrupamento de produtos
pedido_brinde - Itens "brinde" do pedido
pedido_file - Arquivos do pedido
pedido_hist
pedido_itens -
saida_ambiente - Refactor para um campo de observação no pedido
saida_parc - Parcelas do pedido
tipo_real - Como a venda foi realizada

voucher - Cupom de desconto
vourcher_itens - Itens com direito ao voucher
vourcher_uso - Em que foram usados

# Troca
troca - Pedido de troca
troca_itens
troca_hist

# Pet
pet_animal
pet_especie
pet_os_config - Tela os pet
pet_raca

## Sms
sms - Armazena sms enviados

# Tabela de Preço
tabela - Tabelas criadas para enviar por email
tabela_item

# Tarefas
tarefa - Lista de tarefa
tarefa_config - Tela
tarefa_info - Historico

## Usuarios
usuario
usuario_acesso
usuario_log

# Ideias
Importador de configuração de pontos para usuário
